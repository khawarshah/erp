jQuery(document).on("change", ".btn-filter-shop", function(){
	var shop_type = jQuery(".shop_type").val();
	var districts = jQuery(".districts").val();
	var blocks = jQuery(".blocks").val();
	var superviser = "false";
	var html = "";
	if (jQuery("#superviser").prop('checked')==true){ 
        superviser = "true";
    }
	jQuery.get(base_url()+'Shop/filter/'+shop_type+'/'+districts+'/'+blocks+'/'+superviser, 
		function(data) {
			jQuery.each(jQuery.parseJSON(data), function(index, val) {
				html += ' 	<tr>\
							    <th scope="row"><input type="checkbox" checked class="checkbox_id" name="selector[]" value="'+ val.as_shop_id +'"></th>\
							    <td>'+ val.as_shop_name +'</td>\
							    <td>'+ val.as_shop_type +'</td>\
							    <td>'+ val.as_shop_description +'</td>\
							    <td>'+ val.as_shop_district +'</td>\
							    <td>'+ val.as_shop_block +'</td>\
						    </tr>';
			});
			jQuery(".filtered-data").html(html);
		}
	);
	
});