-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 19, 2017 at 01:38 AM
-- Server version: 5.6.36-82.1-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `protonef_main_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `loc_pak_city`
--

CREATE TABLE `loc_pak_city` (
  `ID` int(10) UNSIGNED NOT NULL,
  `city` text,
  `district` int(5) DEFAULT NULL,
  `province` int(5) DEFAULT NULL,
  `zone` int(5) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `loc_pak_city`
--

INSERT INTO `loc_pak_city` (`ID`, `city`, `district`, `province`, `zone`, `created`) VALUES
(1, 'Bahawalpur', 1, 1, 1, '2017-09-01 12:29:59'),
(2, 'Faisalabad', 2, 1, 1, '2017-09-01 12:29:59'),
(3, 'Gujrawala', 3, 1, 1, '2017-09-01 12:29:59'),
(4, 'Islamabad', 4, 1, 1, '2017-09-01 12:29:59'),
(5, 'Lahore', 5, 1, 1, '2017-09-01 12:29:59'),
(6, 'Peshawar', 6, 2, 1, '2017-09-01 12:29:59'),
(7, 'Karachi', 7, 3, 2, '2017-09-01 12:29:59');

-- --------------------------------------------------------

--
-- Table structure for table `loc_pak_dist`
--

CREATE TABLE `loc_pak_dist` (
  `ID` int(11) UNSIGNED NOT NULL,
  `govt_id` int(5) DEFAULT NULL,
  `admin_unit` int(5) DEFAULT NULL,
  `district` text,
  `province` int(5) DEFAULT NULL,
  `zone` int(5) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `loc_pak_dist`
--

INSERT INTO `loc_pak_dist` (`ID`, `govt_id`, `admin_unit`, `district`, `province`, `zone`, `created`) VALUES
(1, NULL, NULL, 'Bahawalpur', 1, 1, '2017-09-01 11:35:13'),
(2, NULL, NULL, 'Faisalabad', 1, 1, '2017-09-01 11:35:13'),
(3, NULL, NULL, 'Gujrawala', 1, 1, '2017-09-01 11:35:13'),
(4, NULL, NULL, 'Islamabad', 1, 1, '2017-09-01 11:35:13'),
(5, NULL, NULL, 'Lahore', 1, 1, '2017-09-01 11:35:13'),
(6, NULL, NULL, 'Peshawar', 2, 1, '2017-09-01 11:35:13'),
(7, NULL, NULL, 'Karachi ', 3, 2, '2017-09-01 11:35:13');

-- --------------------------------------------------------

--
-- Table structure for table `route`
--

CREATE TABLE `route` (
  `ID` int(10) UNSIGNED NOT NULL,
  `route` varchar(255) DEFAULT NULL,
  `favorite` int(11) DEFAULT NULL,
  `supervisor` int(11) DEFAULT NULL,
  `asm` int(11) DEFAULT NULL,
  `district` int(11) DEFAULT NULL,
  `city` int(11) DEFAULT NULL,
  `distance` int(11) DEFAULT NULL,
  `optimized` int(11) DEFAULT NULL,
  `keywords` text,
  `description` text,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `edit` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_by` text,
  `edited_by` text,
  `instructions` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `route`
--

INSERT INTO `route` (`ID`, `route`, `favorite`, `supervisor`, `asm`, `district`, `city`, `distance`, `optimized`, `keywords`, `description`, `created_on`, `edit`, `created_by`, `edited_by`, `instructions`) VALUES
(5, 'Gulshan Iqbal', NULL, 2, 60, 7, 7, 100, NULL, 'This shop will re open at 12pm', 'This shop will re open at 12pm', '2017-10-07 08:05:28', NULL, NULL, NULL, NULL),
(6, 'Lahore', NULL, 2, 61, 5, 5, 20, NULL, 'This shop will re open at 1 pm', 'This shop will re open at 1 pm', '2017-10-07 08:07:45', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `route_assignment`
--

CREATE TABLE `route_assignment` (
  `id` int(11) NOT NULL,
  `merchandiser_id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `week` enum('1','2','3','4') NOT NULL,
  `day` enum('1','2','3','4','5','6','7') NOT NULL,
  `routine_repeat` enum('weekly','monthly','yearly','period','custom_edit') NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `route_assignment`
--

INSERT INTO `route_assignment` (`id`, `merchandiser_id`, `route_id`, `week`, `day`, `routine_repeat`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(26, 33, 5, '1', '1', 'weekly', '2017-10-17', '2017-12-28', '2017-10-13 22:09:06', '2017-10-13 22:09:06'),
(27, 33, 5, '2', '1', 'weekly', '2017-10-17', '2017-12-28', '2017-10-13 22:09:06', '2017-10-13 22:09:06'),
(28, 33, 5, '3', '1', 'weekly', '2017-10-17', '2017-12-28', '2017-10-13 22:09:06', '2017-10-13 22:09:06'),
(29, 33, 5, '4', '1', 'weekly', '2017-10-17', '2017-12-28', '2017-10-13 22:09:06', '2017-10-13 22:09:06'),
(30, 30, 5, '1', '2', 'weekly', '2017-10-21', '2017-12-22', '2017-10-14 11:02:25', '2017-10-14 11:02:25'),
(31, 30, 5, '2', '2', 'weekly', '2017-10-21', '2017-12-22', '2017-10-14 11:02:25', '2017-10-14 11:02:25'),
(32, 30, 5, '3', '2', 'weekly', '2017-10-21', '2017-12-22', '2017-10-14 11:02:25', '2017-10-14 11:02:25');

-- --------------------------------------------------------

--
-- Table structure for table `shop`
--

CREATE TABLE `shop` (
  `ID` int(10) UNSIGNED NOT NULL,
  `shop_name` text,
  `description` text,
  `lat` varchar(255) DEFAULT NULL,
  `long` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shop`
--

INSERT INTO `shop` (`ID`, `shop_name`, `description`, `lat`, `long`) VALUES
(1, 'Brilliant Stores', 'khayaban-e-tipu sultan', '24.765521', '67.075739'),
(2, 'Sind Stores', '', '', ''),
(3, 'Abdul Ghafoor', '', '', ''),
(4, 'Sharif Stores', '', '', ''),
(5, 'Ahmed Stores', '', '', ''),
(6, 'Khan Traders', '', '', ''),
(7, 'Grey Trading', '', '', ''),
(8, 'Zee Traders', '', '', ''),
(9, 'Jamal Trading', '', '', ''),
(10, 'Faraz Trading', '', '', ''),
(11, 'Lahore Stores', '', '', ''),
(12, 'Pak Trading', '', '', ''),
(13, 'Hasan Shakoor', '', '', ''),
(14, 'Baloch Works', '', '', ''),
(15, 'National Stores', '', '', ''),
(16, 'Alladin Shop', '', '', ''),
(17, 'The Family SHOP', '', '', ''),
(18, 'Shayan G Store', '', '', ''),
(19, 'GOLDEN BAKERY', '', '', ''),
(20, 'Bhai Bhai Store', '', '', ''),
(21, 'ZAREEF STORE', '', '', ''),
(22, 'SAQIB SMART GENERAL STORE', '', '', ''),
(23, 'KHATTAK STORE', '', '', ''),
(24, 'Rizwan GS', '', '', ''),
(25, 'AMIR G/S', '', '', ''),
(26, 'Asim GS', '', '', ''),
(27, 'BISMILLAH G/S', '', '', ''),
(28, 'FAROOQ STORE', '', '', ''),
(29, 'Shakir Store', '', '', ''),
(30, 'SUKHI GENERAL STORE', '', '', ''),
(31, 'STAR M/S', '', '', ''),
(32, 'AL IKHLAS CASH AND CARRY', '', '', ''),
(33, 'Khyber Gs', '', '', ''),
(34, '786 GS', '', '', ''),
(35, 'Haq Bahoo GS', '', '', ''),
(36, 'ABBAS G/S', '', '', ''),
(37, 'SYED ABRAR TRADERS', '', '', ''),
(38, 'AKHAWAN FAMILY MART', '', '', ''),
(39, 'NAMAT KHATTAK', '', '', ''),
(40, 'Insaaf GS', '', '', ''),
(41, 'Dilawar Khan K/F', '', '', ''),
(42, 'Imtiaz Store', '', '', ''),
(43, 'Kojack Bky', '', '', ''),
(44, 'Khan G/s', '', '', ''),
(45, 'A to Z G /S', '', '', ''),
(46, 'PAY AND PICK', '', '', ''),
(47, 'Bashir Traders', '', '', ''),
(48, 'Ali Stores', '', '', ''),
(49, 'HAFIZ SUPER STORE', '', '', ''),
(50, 'FOOD FESTIVAL', '', '', ''),
(51, 'Khalid Super Store', '', '', ''),
(52, 'Punjab Trading', '', '', ''),
(53, 'Bhatti Store', '', '', ''),
(54, 'Gawadar Utility', '', '', ''),
(55, 'Ahsan Rahim', '', '', ''),
(56, 'shah jee g/s', 'khayaban-e-tipu sultan', '24.765721', '67.075739'),
(57, 'REHMAN BAKERS', 'khayaban-e-saqib', '24.753529', '67.083592'),
(58, 'New Ali Traders', 'khayaban-e-faisal', '24.767318', '67.071877'),
(59, 'Maamoor Store', 'khayaban-e-ghalib', '24.775072', '67.070546'),
(60, 'JABIS GRAIN CENTER', 'khayaban-e-babar', '24.783411', '67.082133'),
(61, 'N.M STORE', 'zulfiqar street 1', '24.786333', '67.088656'),
(62, 'Jan Mart', 'khayaban-e-hafiz', '24.804799', '67.066941'),
(63, 'S.A Milk', 'khayaban-e-muhafiz', '24.810097', '67.075438'),
(64, 'Dulazak Store', 'khayaban-e-bukhari', '24.805072', '67.074924'),
(65, 'Jilani Store', 'khayaban-e-ghazi', '24.802111', '67.071834'),
(66, 'NADEEM CONF', 'sharah faisal', '24.864491', '67.075138'),
(67, 'Moh Assif G/S K/F', 'sharah faisal', '24.874984', '67.071855'),
(68, 'AZHAR STORE', 'saheed-e-millat', '24.866944', '67.081532'),
(69, 'Chishti K/s', 'shahrah-e-Qaideen', '24.860299', '67.062392'),
(70, 'ASAD GENERAL STORE', 'Allama Shabbir Ahmed usmani Rd', '24.934611', '67.104149'),
(71, 'KHYBER BAKERS', 'shahrah-e-Qaideen', '24.866782', '67.054008'),
(72, 'Good Luck SS', 'Allama Shabbir Ahmed usmani Rd', '24.932379', '67.100608'),
(73, 'Ghousia G/S', 'Allama Shabbir Ahmed usmani Rd', '24.926989', '67.093613'),
(74, 'KDS STORE', 'shahrah-e-Qaideen', '24.869216', '67.050505'),
(75, 'Sajid SS', 'Allama Shabbir Ahmed usmani Rd', '24.923208', '67.089386'),
(76, 'ROYAL MART', 'Allama Shabbir Ahmed usmani Rd', '24.919189', '67.084419'),
(77, 'SYED MUZAMMAL BAKAERS', 'sir shah muhammad suleman rd', '24.909008', '67.055783'),
(78, 'ATIQUE GENERAL STORE', 'sir shah muhammad suleman rd', '24.908457', '67.060976'),
(79, 'JADOON SUPER STORE', 'Sher Shah Suri Rd', '24.958625', '67.061234'),
(80, 'KDS STORE', 'sir shah muhammad suleman rd', '24.904515', '67.068143'),
(81, 'Ghousia G/S', 'Sher Shah Suri Rd', '24.953178', '67.056363'),
(82, 'ASAD GENERAL STORE', 'Sher Shah Suri Rd', '24.942069', '67.046256'),
(83, 'SYED MUZAMMAL BAKAERS', 'sir shah muhammad suleman rd', '24.898109', '67.077477'),
(84, 'ATIQUE GENERAL STORE', 'Rashid minhas Rd', '24.930803', '67.085974'),
(85, 'ROYAL MART', 'Sher Shah Suri Rd', '24.961835', '67.069323'),
(86, 'Good Luck SS', 'Sher Shah Suri Rd', '24.947478', '67.051105'),
(87, 'Sajid SS', 'Sher Shah Suri Rd', '24.932418', '67.036922'),
(88, 'KHYBER BAKERS', 'Rashid minhas Rd', '24.947341', '67.082455'),
(89, 'JADOON SUPER STORE', 'Sher Shah Suri Rd', '24.925802', '67.033253'),
(90, 'ASAD GENERAL STORE', 'Sher Shah Suri Rd', '24.927573', '67.032115'),
(91, 'KHYBER BAKERS', 'Rashid minhas Rd', '24.951758', '67.079431'),
(92, 'Good Luck SS', 'Shahrah Noor Jehan', '24.930667', '67.051105'),
(93, 'Ghousia G/S', 'Shahrah Noor Jehan', '24.934695', '67.029111'),
(94, 'KDS STORE', 'Rashid minhas Rd', '24.961835', '67.069323'),
(95, 'Sajid SS', 'Shahrah Noor Jehan', '24.946232', '67.039239'),
(96, 'ROYAL MART', 'Shahrah Noor Jehan', '24.956816', '67.048337'),
(97, 'SYED MUZAMMAL BAKAERS', 'Rashid minhas Rd', '24.926561', '67.088828'),
(98, 'ATIQUE GENERAL STORE', 'Rashid minhas Rd', '24.919225', '67.095265'),
(99, 'JADOON SUPER STORE', 'Allama Rasheed Turabi  Road', '24.950474', '67.061706'),
(100, 'HAFIZ  G/S', 'Allama Rasheed Turabi  Road', '24.927106', '67.047351'),
(101, 'SUPER NICE BAKERY', 'Petrol Pump', '24.911098', '67.028587'),
(102, 'NIGHAR DEPT.STORE', 'Petrol Pump', '24.91256', '67.032086'),
(103, 'AL-MACCA SHOPING.', 'Petrol Pump', '24.910711', '67.033931'),
(104, 'GOURMET BAKERS', 'Allama Rasheed Turabi  Road', '24.941719', '67.060741'),
(105, 'Insaf K/F', 'Allama Rasheed Turabi  Road', '24.939559', '67.052372'),
(106, 'Geo Cash & Carry', '', '', ''),
(107, 'SUALEH BHAI STORE', '', '', ''),
(108, 'CAFE NO1 CANTEEN', '', '', ''),
(109, 'AL MADINA G/S', '', '', ''),
(110, 'KHALID GENERAL STORE', '', '', ''),
(111, 'NEW USMAN STORE', '', '', ''),
(112, 'AL QAMAR GENERAL STORE', '', '', ''),
(113, 'HASSNAN DEPARTMENTAL STORE', '', '', ''),
(114, 'AL BARKAT STORE', '', '', ''),
(115, 'NIAZ STORE', '', '', ''),
(116, 'Waseem Bakers', '', '', ''),
(117, 'SHAKEEL BROTHERS & STORE', '', '', ''),
(118, 'YUMZ BK', '', '', ''),
(119, 'abdul hameed g/s', '', '', ''),
(120, 'AL RASHEED BAKERY', '', '', ''),
(121, 'AL SADAAT FRUIT SHOP', '', '', ''),
(122, 'sabir G/S', '', '', ''),
(123, 'HAVEN STORE', '', '', ''),
(124, 'New ashraf bakers', '', '', ''),
(125, 'Sheikh Super Store', '', '', ''),
(126, 'ARSALAN G/S', '', '', ''),
(127, 'Haider Bakery', '', '', ''),
(128, 'Barani College Fair Price Shop', '', '', ''),
(129, 'Aftaab Khattak GS', '', '', ''),
(130, 'ZAIB MEDICAL STORE', '', '', ''),
(131, 'KOH-E-NOOR STORE', '', '', ''),
(132, 'SHAH JEE K/S', '', '', ''),
(133, 'MASHALLAH STORE', '', '', ''),
(134, 'Baba G/Store', '', '', ''),
(135, 'FAZAL ELAHI KARYANA STORE', '', '', ''),
(136, 'SULTANIA GEN.STORE', '', '', ''),
(137, 'Welcome GS', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `shop_loc_web`
--

CREATE TABLE `shop_loc_web` (
  `ID` int(11) UNSIGNED NOT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `map_tag` text,
  `market` text,
  `village_goth_area` text,
  `street` text,
  `block` varchar(255) DEFAULT NULL,
  `town` text,
  `district` int(11) DEFAULT NULL,
  `city` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shop_loc_web`
--

INSERT INTO `shop_loc_web` (`ID`, `shop_id`, `map_tag`, `market`, `village_goth_area`, `street`, `block`, `town`, `district`, `city`) VALUES
(1, 1, 'Multan Map', 'Multan market', 'Multan area', 'Multan street', 'Multan blockRoad', 'Multani town', 1, 1),
(2, 2, NULL, NULL, NULL, NULL, 'Multan Road', 'Bahawalpur', 1, 1),
(3, 3, NULL, NULL, NULL, NULL, 'Multan Road', 'Bahawalpur', 1, 1),
(4, 4, NULL, NULL, NULL, NULL, 'Multan Road', 'Bahawalpur', 1, 1),
(5, 5, NULL, NULL, NULL, NULL, 'Bahawalpur Road', 'Bahawalpur', 1, 1),
(6, 6, NULL, NULL, NULL, NULL, 'Bahawalpur Road', 'Bahawalpur', 1, 1),
(7, 7, NULL, NULL, NULL, NULL, 'Bahawalpur Road', 'Bahawalpur', 1, 1),
(8, 8, NULL, NULL, NULL, NULL, 'Bahawalpur Road', 'Bahawalpur', 1, 1),
(9, 9, NULL, NULL, NULL, NULL, 'Bahawalpur Road', 'Bahawalpur', 1, 1),
(10, 10, NULL, NULL, NULL, NULL, 'D Ground', 'Faisalabad', 2, 2),
(11, 11, NULL, NULL, NULL, NULL, 'D Ground', 'Faisalabad', 2, 2),
(12, 12, NULL, NULL, NULL, NULL, 'D Ground', 'Faisalabad', 2, 2),
(13, 13, NULL, NULL, NULL, NULL, 'Madina Town', 'Faisalabad', 2, 2),
(14, 14, NULL, NULL, NULL, NULL, 'Madina Town', 'Faisalabad', 2, 2),
(15, 15, NULL, NULL, NULL, NULL, 'Liaquatabad', 'Faisalabad', 2, 2),
(16, 16, NULL, NULL, NULL, NULL, 'Liaquatabad', 'Faisalabad', 2, 2),
(17, 17, NULL, NULL, NULL, NULL, 'GT Road', 'Gujrawala', 3, 3),
(18, 18, NULL, NULL, NULL, NULL, 'GT Road', 'Gujrawala', 3, 3),
(19, 19, NULL, NULL, NULL, NULL, 'GT Road', 'Gujrawala', 3, 3),
(20, 20, NULL, NULL, NULL, NULL, 'Sheikhupura Road', 'Gujrawala', 3, 3),
(21, 21, NULL, NULL, NULL, NULL, 'Sialkot Rd', 'Gujrawala', 3, 3),
(22, 22, NULL, NULL, NULL, NULL, 'Sialkot Rd', 'Gujrawala', 3, 3),
(23, 23, NULL, NULL, NULL, NULL, 'F10 Markaz', 'Islamabad', 4, 4),
(24, 24, NULL, NULL, NULL, NULL, 'F10 Markaz', 'Islamabad', 4, 4),
(25, 25, NULL, NULL, NULL, NULL, 'Jinnah Super', 'Islamabad', 4, 4),
(26, 26, NULL, NULL, NULL, NULL, 'Jinnah Super', 'Islamabad', 4, 4),
(27, 27, NULL, NULL, NULL, NULL, 'Saddar', 'Rawalpindi', 4, 4),
(28, 28, NULL, NULL, NULL, NULL, 'Saddar', 'Rawalpindi', 4, 4),
(29, 29, NULL, NULL, NULL, NULL, 'Saddar', 'Rawalpindi', 4, 4),
(30, 30, NULL, NULL, NULL, NULL, 'Food street', 'Mall Road', 5, 5),
(31, 31, NULL, NULL, NULL, NULL, 'Food street', 'Mall Road', 5, 5),
(32, 32, NULL, NULL, NULL, NULL, 'Food street', 'Mall Road', 5, 5),
(33, 33, NULL, NULL, NULL, NULL, 'Food street', 'Mall Road', 5, 5),
(34, 34, NULL, NULL, NULL, NULL, 'Food street', 'Mall Road', 5, 5),
(35, 35, NULL, NULL, NULL, NULL, 'Fortess', 'Fortess', 5, 5),
(36, 36, NULL, NULL, NULL, NULL, 'Fortess', 'Fortess', 5, 5),
(37, 37, NULL, NULL, NULL, NULL, 'Fortess', 'Fortess', 5, 5),
(38, 38, NULL, NULL, NULL, NULL, 'Fortess', 'Fortess', 5, 5),
(39, 39, NULL, NULL, NULL, NULL, 'Cavalry', 'Gulberg', 5, 5),
(40, 40, NULL, NULL, NULL, NULL, 'Cavalry', 'Gulberg', 5, 5),
(41, 41, NULL, NULL, NULL, NULL, 'Gulberg 2', 'Gulberg', 5, 5),
(42, 42, NULL, NULL, NULL, NULL, 'Gulberg 2', 'Gulberg', 5, 5),
(43, 43, NULL, NULL, NULL, NULL, 'Gulberg 3', 'Gulberg', 5, 5),
(44, 44, NULL, NULL, NULL, NULL, 'Gulberg 3', 'Gulberg', 5, 5),
(45, 45, NULL, NULL, NULL, NULL, 'Gulberg 3', 'Gulberg', 5, 5),
(46, 46, NULL, NULL, NULL, NULL, 'Gulberg 2', 'Gulberg', 5, 5),
(47, 47, NULL, NULL, NULL, NULL, 'Hayatabad', 'Peshawar', 6, 6),
(48, 48, NULL, NULL, NULL, NULL, 'Hayatabad', 'Peshawar', 6, 6),
(49, 49, NULL, NULL, NULL, NULL, 'Charsadda', 'Peshawar', 6, 6),
(50, 50, NULL, NULL, NULL, NULL, 'Charsadda', 'Peshawar', 6, 6),
(51, 51, NULL, NULL, NULL, NULL, 'Charsadda', 'Peshawar', 6, 6),
(52, 52, NULL, NULL, NULL, NULL, 'Charsadda', 'Peshawar', 6, 6),
(53, 53, NULL, NULL, NULL, NULL, 'University Town', 'Peshawar', 6, 6),
(54, 54, NULL, NULL, NULL, NULL, 'University Town', 'Peshawar', 6, 6),
(55, 55, NULL, NULL, NULL, NULL, 'University Town', 'Peshawar', 6, 6),
(56, 56, NULL, NULL, NULL, NULL, ' dha_phase8', 'dha', 7, 7),
(57, 57, NULL, NULL, NULL, NULL, ' dha_phase8', 'dha', 7, 7),
(58, 58, NULL, NULL, NULL, NULL, ' dha_phase8', 'dha', 7, 7),
(59, 59, NULL, NULL, NULL, NULL, ' dha_phase8', 'dha', 7, 7),
(60, 60, NULL, NULL, NULL, NULL, ' dha_phase8', 'dha', 7, 7),
(61, 61, NULL, NULL, NULL, NULL, ' dha_phase8', 'dha', 7, 7),
(62, 62, NULL, NULL, NULL, NULL, ' dha_phase6', 'dha', 7, 7),
(63, 63, NULL, NULL, NULL, NULL, ' dha_phase6', 'dha', 7, 7),
(64, 64, NULL, NULL, NULL, NULL, ' dha_phase6', 'dha', 7, 7),
(65, 65, NULL, NULL, NULL, NULL, ' dha_phase6', 'dha', 7, 7),
(66, 66, NULL, NULL, NULL, NULL, 'pechs_block6', 'pechs', 7, 7),
(67, 67, NULL, NULL, NULL, NULL, 'pechs_block6', 'pechs', 7, 7),
(68, 68, NULL, NULL, NULL, NULL, 'pechs_block6', 'pechs', 7, 7),
(69, 69, NULL, NULL, NULL, NULL, 'pechs_block6', 'pechs', 7, 7),
(70, 70, NULL, NULL, NULL, NULL, 'gulshan_block 4', 'Gulshan-e-iqbal', 7, 7),
(71, 71, NULL, NULL, NULL, NULL, 'pechs_block6', 'pechs', 7, 7),
(72, 72, NULL, NULL, NULL, NULL, 'gulshan_block 4', 'Gulshan-e-iqbal', 7, 7),
(73, 73, NULL, NULL, NULL, NULL, 'gulshan_block 3', 'Gulshan-e-iqbal', 7, 7),
(74, 74, NULL, NULL, NULL, NULL, 'pechs_block2', 'pechs', 7, 7),
(75, 75, NULL, NULL, NULL, NULL, 'gulshan_block 2', 'Gulshan-e-iqbal', 7, 7),
(76, 76, NULL, NULL, NULL, NULL, 'gulshan_block 1', 'Gulshan-e-iqbal', 7, 7),
(77, 77, NULL, NULL, NULL, NULL, 'sharifabad', 'liaqutabad', 7, 7),
(78, 78, NULL, NULL, NULL, NULL, 'FB Area_block 2', 'liaqutabad', 7, 7),
(79, 79, NULL, NULL, NULL, NULL, 'shadman town', 'North Nazimabad', 7, 7),
(80, 80, NULL, NULL, NULL, NULL, 'gulshan_block 13-A', 'liaqutabad', 7, 7),
(81, 81, NULL, NULL, NULL, NULL, 'N. Naz. block J ', 'North Nazimabad', 7, 7),
(82, 82, NULL, NULL, NULL, NULL, 'N. Naz. block D', 'North Nazimabad', 7, 7),
(83, 83, NULL, NULL, NULL, NULL, 'gulshan_block 17', 'liaqutabad', 7, 7),
(84, 84, NULL, NULL, NULL, NULL, 'gulshan_block 15', 'Gulshan-e-iqbal', 7, 7),
(85, 85, NULL, NULL, NULL, NULL, 'N. Naz. block E', 'North Nazimabad', 7, 7),
(86, 86, NULL, NULL, NULL, NULL, 'N. Naz. block K', 'North Nazimabad', 7, 7),
(87, 87, NULL, NULL, NULL, NULL, 'N. Naz. block C', 'North Nazimabad', 7, 7),
(88, 88, NULL, NULL, NULL, NULL, 'Ancholi', 'Gulshan-e-iqbal', 7, 7),
(89, 89, NULL, NULL, NULL, NULL, 'N. Naz. block B', 'North Nazimabad', 7, 7),
(90, 90, NULL, NULL, NULL, NULL, 'N. Naz. block A', 'North Nazimabad', 7, 7),
(91, 91, NULL, NULL, NULL, NULL, 'Al Noor Society', 'Gulshan-e-iqbal', 7, 7),
(92, 92, NULL, NULL, NULL, NULL, 'N. Naz. block A', 'North Nazimabad', 7, 7),
(93, 93, NULL, NULL, NULL, NULL, 'N. Naz. block A', 'North Nazimabad', 7, 7),
(94, 94, NULL, NULL, NULL, NULL, 'north karachi', 'North Nazimabad', 7, 7),
(95, 95, NULL, NULL, NULL, NULL, 'N. Naz. block D', 'North Nazimabad', 7, 7),
(96, 96, NULL, NULL, NULL, NULL, 'N. Naz. block J', 'North Nazimabad', 7, 7),
(97, 97, NULL, NULL, NULL, NULL, 'gulshan_block 2', 'Gulshan-e-iqbal', 7, 7),
(98, 98, NULL, NULL, NULL, NULL, 'gulshan_block 13', 'Gulshan-e-iqbal', 7, 7),
(99, 99, NULL, NULL, NULL, NULL, 'N. Naz. block L', 'North Nazimabad', 7, 7),
(100, 100, NULL, NULL, NULL, NULL, 'N. Naz. block F', 'North Nazimabad', 7, 7),
(101, 101, NULL, NULL, NULL, NULL, '1 number', 'Nazimabad', 7, 7),
(102, 102, NULL, NULL, NULL, NULL, '1 number', 'Nazimabad', 7, 7),
(103, 103, NULL, NULL, NULL, NULL, '1 number', 'Nazimabad', 7, 7),
(104, 104, NULL, NULL, NULL, NULL, 'N. Naz. block M', 'North Nazimabad', 7, 7),
(105, 105, NULL, NULL, NULL, NULL, 'N. Naz. block N', 'North Nazimabad', 7, 7),
(106, 106, NULL, NULL, NULL, NULL, '7 number', 'Nazimabad', 7, 7),
(107, 107, NULL, NULL, NULL, NULL, '7 number', 'Nazimabad', 7, 7),
(108, 108, NULL, NULL, NULL, NULL, '7 number', 'Nazimabad', 7, 7),
(109, 109, NULL, NULL, NULL, NULL, '7 number', 'Nazimabad', 7, 7),
(110, 110, NULL, NULL, NULL, NULL, '7 number', 'Nazimabad', 7, 7),
(111, 111, NULL, NULL, NULL, NULL, '7 number', 'Nazimabad', 7, 7),
(112, 112, NULL, NULL, NULL, NULL, '7 number', 'Nazimabad', 7, 7),
(113, 113, NULL, NULL, NULL, NULL, '7 number', 'Nazimabad', 7, 7),
(114, 114, NULL, NULL, NULL, NULL, '7 number', 'Nazimabad', 7, 7),
(115, 115, NULL, NULL, NULL, NULL, '7 number', 'Nazimabad', 7, 7),
(116, 116, NULL, NULL, NULL, NULL, '7 number', 'Nazimabad', 7, 7),
(117, 117, NULL, NULL, NULL, NULL, '7 number', 'Nazimabad', 7, 7),
(118, 118, NULL, NULL, NULL, NULL, '7 number', 'Nazimabad', 7, 7),
(119, 119, NULL, NULL, NULL, NULL, '1 number', 'Nazimabad', 7, 7),
(120, 120, NULL, NULL, NULL, NULL, '1 number', 'Nazimabad', 7, 7),
(121, 121, NULL, NULL, NULL, NULL, '1 number', 'Nazimabad', 7, 7),
(122, 122, NULL, NULL, NULL, NULL, '1 number', 'Nazimabad', 7, 7),
(123, 123, NULL, NULL, NULL, NULL, '1 number', 'Nazimabad', 7, 7),
(124, 124, NULL, NULL, NULL, NULL, '2 number', 'Nazimabad', 7, 7),
(125, 125, NULL, NULL, NULL, NULL, '2 number', 'Nazimabad', 7, 7),
(126, 126, NULL, NULL, NULL, NULL, '2 number', 'Nazimabad', 7, 7),
(127, 127, NULL, NULL, NULL, NULL, '2 number', 'Nazimabad', 7, 7),
(128, 128, NULL, NULL, NULL, NULL, '2 number', 'Nazimabad', 7, 7),
(129, 129, NULL, NULL, NULL, NULL, '2 number', 'Nazimabad', 7, 7),
(130, 130, NULL, NULL, NULL, NULL, '2 number', 'Nazimabad', 7, 7),
(131, 131, NULL, NULL, NULL, NULL, 'Regal', 'Saddar', 7, 7),
(132, 132, NULL, NULL, NULL, NULL, 'Regal', 'Saddar', 7, 7),
(133, 133, NULL, NULL, NULL, NULL, 'Regal', 'Saddar', 7, 7),
(134, 134, NULL, NULL, NULL, NULL, 'Regal', 'Saddar', 7, 7),
(135, 135, NULL, NULL, NULL, NULL, 'Regal', 'Saddar', 7, 7),
(136, 136, NULL, NULL, NULL, NULL, 'Regal', 'Saddar', 7, 7),
(137, 137, NULL, NULL, NULL, NULL, 'Regal', 'Saddar', 7, 7);

-- --------------------------------------------------------

--
-- Table structure for table `shop_managers`
--

CREATE TABLE `shop_managers` (
  `ID` int(10) UNSIGNED NOT NULL,
  `shop` int(11) DEFAULT NULL,
  `supervisor` int(11) DEFAULT NULL,
  `asm` int(11) DEFAULT NULL,
  `merchandiser` int(11) DEFAULT NULL,
  `backup_merchandiser` int(11) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shop_managers`
--

INSERT INTO `shop_managers` (`ID`, `shop`, `supervisor`, `asm`, `merchandiser`, `backup_merchandiser`, `comments`) VALUES
(1, 23, 1, 1, NULL, NULL, NULL),
(2, 24, 1, 1, NULL, NULL, NULL),
(3, 62, 2, 2, NULL, NULL, NULL),
(4, 63, 2, 2, NULL, NULL, NULL),
(5, 70, 3, 3, NULL, NULL, NULL),
(6, 72, 3, 3, NULL, NULL, NULL),
(7, 73, 3, 3, NULL, NULL, NULL),
(8, 75, 3, 3, NULL, NULL, NULL),
(9, 76, 3, 3, NULL, NULL, NULL),
(10, 77, 3, 3, NULL, NULL, NULL),
(11, 78, 3, 3, NULL, NULL, NULL),
(12, 79, 3, 3, NULL, NULL, NULL),
(13, 10, 4, 4, NULL, NULL, NULL),
(14, 11, 4, 4, NULL, NULL, NULL),
(15, 12, 4, 4, NULL, NULL, NULL),
(16, 25, 4, 4, NULL, NULL, NULL),
(17, 26, 4, 4, NULL, NULL, NULL),
(18, 13, 5, 5, NULL, NULL, NULL),
(19, 14, 5, 5, NULL, NULL, NULL),
(20, 64, 6, 6, NULL, NULL, NULL),
(21, 65, 6, 6, NULL, NULL, NULL),
(22, 47, 7, 7, NULL, NULL, NULL),
(23, 48, 7, 7, NULL, NULL, NULL),
(24, 66, 8, 8, NULL, NULL, NULL),
(25, 67, 8, 8, NULL, NULL, NULL),
(26, 68, 8, 8, NULL, NULL, NULL),
(27, 69, 8, 8, NULL, NULL, NULL),
(28, 71, 8, 8, NULL, NULL, NULL),
(29, 74, 8, 8, NULL, NULL, NULL),
(30, 35, 9, 9, NULL, NULL, NULL),
(31, 36, 9, 9, NULL, NULL, NULL),
(32, 37, 9, 9, NULL, NULL, NULL),
(33, 38, 9, 9, NULL, NULL, NULL),
(34, 106, 10, 10, NULL, NULL, NULL),
(35, 107, 10, 10, NULL, NULL, NULL),
(36, 108, 10, 10, NULL, NULL, NULL),
(37, 109, 10, 10, NULL, NULL, NULL),
(38, 110, 10, 10, NULL, NULL, NULL),
(39, 111, 10, 10, NULL, NULL, NULL),
(40, 112, 10, 10, NULL, NULL, NULL),
(41, 113, 10, 10, NULL, NULL, NULL),
(42, 114, 10, 10, NULL, NULL, NULL),
(43, 115, 10, 10, NULL, NULL, NULL),
(44, 116, 10, 10, NULL, NULL, NULL),
(45, 117, 10, 10, NULL, NULL, NULL),
(46, 118, 10, 10, NULL, NULL, NULL),
(47, 17, 11, 11, NULL, NULL, NULL),
(48, 18, 11, 11, NULL, NULL, NULL),
(49, 19, 11, 11, NULL, NULL, NULL),
(50, 39, 12, 12, NULL, NULL, NULL),
(51, 40, 12, 12, NULL, NULL, NULL),
(52, 20, 13, 13, NULL, NULL, NULL),
(53, 101, 14, 14, NULL, NULL, NULL),
(54, 102, 14, 14, NULL, NULL, NULL),
(55, 103, 14, 14, NULL, NULL, NULL),
(56, 119, 14, 14, NULL, NULL, NULL),
(57, 120, 14, 14, NULL, NULL, NULL),
(58, 121, 14, 14, NULL, NULL, NULL),
(59, 122, 14, 14, NULL, NULL, NULL),
(60, 123, 14, 14, NULL, NULL, NULL),
(61, 21, 15, 15, NULL, NULL, NULL),
(62, 22, 15, 15, NULL, NULL, NULL),
(63, 49, 16, 16, NULL, NULL, NULL),
(64, 50, 16, 16, NULL, NULL, NULL),
(65, 51, 16, 16, NULL, NULL, NULL),
(66, 52, 16, 16, NULL, NULL, NULL),
(67, 56, 17, 17, NULL, NULL, NULL),
(68, 57, 17, 17, NULL, NULL, NULL),
(69, 58, 17, 17, NULL, NULL, NULL),
(70, 59, 17, 17, NULL, NULL, NULL),
(71, 60, 17, 17, NULL, NULL, NULL),
(72, 61, 17, 17, NULL, NULL, NULL),
(73, 131, 17, 17, NULL, NULL, NULL),
(74, 132, 17, 17, NULL, NULL, NULL),
(75, 133, 17, 17, NULL, NULL, NULL),
(76, 134, 17, 17, NULL, NULL, NULL),
(77, 135, 17, 17, NULL, NULL, NULL),
(78, 136, 17, 17, NULL, NULL, NULL),
(79, 137, 17, 17, NULL, NULL, NULL),
(80, 1, 18, 18, NULL, NULL, NULL),
(81, 2, 18, 18, NULL, NULL, NULL),
(82, 3, 18, 18, NULL, NULL, NULL),
(83, 4, 18, 18, NULL, NULL, NULL),
(84, 80, 19, 19, NULL, NULL, NULL),
(85, 81, 19, 19, NULL, NULL, NULL),
(86, 82, 19, 19, NULL, NULL, NULL),
(87, 83, 19, 19, NULL, NULL, NULL),
(88, 84, 19, 19, NULL, NULL, NULL),
(89, 85, 19, 19, NULL, NULL, NULL),
(90, 86, 19, 19, NULL, NULL, NULL),
(91, 87, 19, 19, NULL, NULL, NULL),
(92, 88, 19, 19, NULL, NULL, NULL),
(93, 89, 19, 19, NULL, NULL, NULL),
(94, 100, 19, 19, NULL, NULL, NULL),
(95, 41, 20, 20, NULL, NULL, NULL),
(96, 42, 20, 20, NULL, NULL, NULL),
(97, 43, 20, 20, NULL, NULL, NULL),
(98, 44, 20, 20, NULL, NULL, NULL),
(99, 45, 20, 20, NULL, NULL, NULL),
(100, 46, 20, 20, NULL, NULL, NULL),
(101, 5, 21, 21, NULL, NULL, NULL),
(102, 6, 21, 21, NULL, NULL, NULL),
(103, 7, 21, 21, NULL, NULL, NULL),
(104, 8, 21, 21, NULL, NULL, NULL),
(105, 9, 21, 21, NULL, NULL, NULL),
(106, 27, 23, 23, NULL, NULL, NULL),
(107, 28, 23, 23, NULL, NULL, NULL),
(108, 29, 23, 23, NULL, NULL, NULL),
(109, 30, 24, 24, NULL, NULL, NULL),
(110, 31, 24, 24, NULL, NULL, NULL),
(111, 32, 24, 24, NULL, NULL, NULL),
(112, 33, 24, 24, NULL, NULL, NULL),
(113, 15, 25, 25, NULL, NULL, NULL),
(114, 16, 25, 25, NULL, NULL, NULL),
(115, 34, 26, 26, NULL, NULL, NULL),
(116, 90, 26, 26, NULL, NULL, NULL),
(117, 91, 26, 26, NULL, NULL, NULL),
(118, 92, 26, 26, NULL, NULL, NULL),
(119, 93, 26, 26, NULL, NULL, NULL),
(120, 94, 26, 26, NULL, NULL, NULL),
(121, 95, 26, 26, NULL, NULL, NULL),
(122, 96, 26, 26, NULL, NULL, NULL),
(123, 97, 26, 26, NULL, NULL, NULL),
(124, 98, 26, 26, NULL, NULL, NULL),
(125, 99, 26, 26, NULL, NULL, NULL),
(126, 104, 27, 27, NULL, NULL, NULL),
(127, 105, 27, 27, NULL, NULL, NULL),
(128, 53, 28, 28, NULL, NULL, NULL),
(129, 54, 28, 28, NULL, NULL, NULL),
(130, 55, 28, 28, NULL, NULL, NULL),
(131, 124, 29, 29, NULL, NULL, NULL),
(132, 125, 29, 29, NULL, NULL, NULL),
(133, 126, 29, 29, NULL, NULL, NULL),
(134, 127, 29, 29, NULL, NULL, NULL),
(135, 128, 29, 29, NULL, NULL, NULL),
(136, 129, 29, 29, NULL, NULL, NULL),
(137, 130, 29, 29, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shop_route`
--

CREATE TABLE `shop_route` (
  `job_id` int(10) UNSIGNED NOT NULL,
  `user` int(11) DEFAULT NULL,
  `shop` int(11) DEFAULT NULL,
  `route_id` int(11) DEFAULT NULL,
  `client` int(11) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `supervisor` int(11) DEFAULT NULL,
  `entered_by` int(11) DEFAULT NULL,
  `assigned_by` int(11) DEFAULT NULL,
  `jobs_status` int(11) DEFAULT NULL,
  `assigned_date` varchar(0) DEFAULT NULL,
  `urgency` int(11) DEFAULT NULL,
  `rank_m` int(11) DEFAULT NULL,
  `rank_2` int(11) DEFAULT NULL,
  `seq` int(11) DEFAULT NULL,
  `intruction` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shop_route`
--

INSERT INTO `shop_route` (`job_id`, `user`, `shop`, `route_id`, `client`, `date`, `supervisor`, `entered_by`, `assigned_by`, `jobs_status`, `assigned_date`, `urgency`, `rank_m`, `rank_2`, `seq`, `intruction`) VALUES
(31, 3, 63, 5, NULL, '2017-10-16 14:57:22', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL),
(32, 3, 65, 5, NULL, '2017-10-16 14:58:02', NULL, NULL, NULL, NULL, NULL, NULL, 14, 1, NULL, NULL),
(33, NULL, 56, 5, NULL, '2017-10-16 14:58:03', NULL, NULL, NULL, NULL, NULL, NULL, 18, 1, NULL, NULL),
(34, NULL, 57, 5, NULL, '2017-10-16 14:20:49', NULL, NULL, NULL, NULL, NULL, NULL, 21, 1, NULL, NULL),
(35, NULL, 106, 5, NULL, '2017-10-16 14:20:59', NULL, NULL, NULL, NULL, NULL, NULL, 20, 1, NULL, NULL),
(36, NULL, 107, 5, NULL, '2017-10-08 17:54:03', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(37, NULL, 108, 5, NULL, '2017-10-08 17:54:03', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(38, NULL, 109, 5, NULL, '2017-10-08 17:54:03', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(39, NULL, 110, 5, NULL, '2017-10-08 17:54:03', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(40, NULL, 111, 5, NULL, '2017-10-08 17:54:03', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(41, NULL, 112, 5, NULL, '2017-10-08 17:54:03', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(42, NULL, 113, 5, NULL, '2017-10-08 17:54:03', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(43, NULL, 114, 5, NULL, '2017-10-08 17:54:03', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(44, NULL, 115, 5, NULL, '2017-10-08 17:54:03', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(45, NULL, 116, 5, NULL, '2017-10-08 17:54:03', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(46, NULL, 117, 5, NULL, '2017-10-08 17:54:03', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(47, NULL, 118, 5, NULL, '2017-10-08 17:54:03', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(48, NULL, 70, 5, NULL, '2017-10-16 04:35:30', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(49, NULL, 72, 5, NULL, '2017-10-16 04:35:30', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(50, NULL, 73, 5, NULL, '2017-10-16 04:35:30', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(51, NULL, 75, 5, NULL, '2017-10-16 04:35:30', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(52, NULL, 83, 5, NULL, '2017-10-16 04:35:30', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(53, NULL, 97, 5, NULL, '2017-10-16 04:35:30', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(54, NULL, 65, 10, NULL, '2017-10-18 17:00:13', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(55, NULL, 111, 10, NULL, '2017-10-18 17:00:13', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(56, NULL, 118, 10, NULL, '2017-10-18 17:00:13', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(57, NULL, 119, 10, NULL, '2017-10-18 17:00:13', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(58, NULL, 104, 10, NULL, '2017-10-18 17:00:13', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL),
(59, NULL, 63, 11, NULL, '2017-10-18 17:01:02', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL),
(60, NULL, 65, 11, NULL, '2017-10-18 17:01:05', NULL, NULL, NULL, NULL, NULL, NULL, 2, 0, NULL, NULL),
(61, NULL, 56, 11, NULL, '2017-10-18 17:01:08', NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, NULL),
(62, NULL, 57, 11, NULL, '2017-10-18 17:01:11', NULL, NULL, NULL, NULL, NULL, NULL, 4, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shop_type`
--

CREATE TABLE `shop_type` (
  `ID` int(10) UNSIGNED NOT NULL,
  `client` int(11) DEFAULT NULL,
  `shop_type` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shop_type`
--

INSERT INTO `shop_type` (`ID`, `client`, `shop_type`) VALUES
(1, 1, 'mohala store'),
(2, 1, 'supermart'),
(3, 1, 'chainstore'),
(4, 1, 'pharmacy'),
(5, 1, 'coldspot'),
(6, 1, 'resturant'),
(7, 1, 'chaiwala'),
(8, 1, 'school'),
(9, 1, 'canteen');

-- --------------------------------------------------------

--
-- Table structure for table `shop_typewise`
--

CREATE TABLE `shop_typewise` (
  `ID` int(11) NOT NULL,
  `shop` int(11) NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop_typewise`
--

INSERT INTO `shop_typewise` (`ID`, `shop`, `type`) VALUES
(1, 1, 6),
(2, 2, 9),
(3, 3, 8),
(4, 4, 6),
(5, 5, 6),
(6, 6, 5),
(7, 7, 5),
(8, 8, 2),
(9, 9, 7),
(10, 10, 2),
(11, 11, 2),
(12, 12, 8),
(13, 13, 9),
(14, 14, 5),
(15, 15, 1),
(16, 16, 9),
(17, 17, 9),
(18, 18, 1),
(19, 19, 9),
(20, 20, 3),
(21, 21, 8),
(22, 22, 7),
(23, 23, 2),
(24, 24, 3),
(25, 25, 7),
(26, 26, 9),
(27, 27, 9),
(28, 28, 1),
(29, 29, 8),
(30, 30, 3),
(31, 31, 3),
(32, 32, 7),
(33, 33, 9),
(34, 34, 5),
(35, 35, 1),
(36, 36, 9),
(37, 37, 7),
(38, 38, 4),
(39, 39, 3),
(40, 40, 5),
(41, 41, 9),
(42, 42, 2),
(43, 43, 8),
(44, 44, 9),
(45, 45, 4),
(46, 46, 9),
(47, 47, 4),
(48, 48, 1),
(49, 49, 5),
(50, 50, 7),
(51, 51, 2),
(52, 52, 2),
(53, 53, 6),
(54, 54, 6),
(55, 55, 1),
(56, 56, 8),
(57, 57, 8),
(58, 58, 9),
(59, 59, 4),
(60, 60, 6),
(61, 61, 6),
(62, 62, 2),
(63, 63, 8),
(64, 64, 4),
(65, 65, 1),
(66, 66, 9),
(67, 67, 8),
(68, 68, 5),
(69, 69, 5),
(70, 70, 5),
(71, 71, 2),
(72, 72, 6),
(73, 73, 4),
(74, 74, 8),
(75, 75, 7),
(76, 76, 6),
(77, 77, 5),
(78, 78, 7),
(79, 79, 2),
(80, 80, 3),
(81, 81, 3),
(82, 82, 5),
(83, 83, 5),
(84, 84, 4),
(85, 85, 9),
(86, 86, 6),
(87, 87, 6),
(88, 88, 2),
(89, 89, 5),
(90, 90, 2),
(91, 91, 7),
(92, 92, 9),
(93, 93, 8),
(94, 94, 9),
(95, 95, 7),
(96, 96, 6),
(97, 97, 5),
(98, 98, 2),
(99, 99, 5),
(100, 100, 2),
(101, 101, 2),
(102, 102, 9),
(103, 103, 9),
(104, 104, 1),
(105, 105, 8),
(106, 106, 7),
(107, 107, 3),
(108, 108, 7),
(109, 109, 6),
(110, 110, 2),
(111, 111, 1),
(112, 112, 3),
(113, 113, 5),
(114, 114, 7),
(115, 115, 6),
(116, 116, 8),
(117, 117, 2),
(118, 118, 1),
(119, 119, 1),
(120, 120, 9),
(121, 121, 2),
(122, 122, 3),
(123, 123, 4),
(124, 124, 4),
(125, 125, 8),
(126, 126, 6),
(127, 127, 3),
(128, 128, 9),
(129, 129, 5),
(130, 130, 4),
(131, 131, 7),
(132, 132, 8),
(133, 133, 6),
(134, 134, 7),
(135, 135, 5),
(136, 136, 2),
(137, 137, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_login`
--

CREATE TABLE `tbl_login` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `alt_email` varchar(255) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_login`
--

INSERT INTO `tbl_login` (`user_id`, `email`, `alt_email`, `emp_id`, `pass`, `created`) VALUES
(1, 'mmshakirkhan@gmail.com', NULL, 2, '12345', '2017-04-18 08:35:18'),
(2, 'mmshakirkhan@yahoo.com', NULL, 1, '12345', '2017-08-11 16:34:17'),
(3, 'tahir@gmail.com', NULL, 2, '12345', '2017-08-11 16:34:47');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `ID` int(10) UNSIGNED NOT NULL,
  `emp_id` varchar(6) DEFAULT NULL,
  `name` text,
  `role` int(6) DEFAULT NULL,
  `mobile_1` decimal(6,0) DEFAULT NULL,
  `mobile_2` decimal(6,0) DEFAULT NULL,
  `manager` int(6) DEFAULT NULL,
  `team` varchar(6) DEFAULT NULL,
  `committee` varchar(6) DEFAULT NULL,
  `dept` int(6) DEFAULT NULL,
  `zone` int(6) DEFAULT NULL,
  `division` int(6) DEFAULT NULL,
  `district` int(6) DEFAULT NULL,
  `city` int(6) DEFAULT NULL,
  `town` int(6) DEFAULT NULL,
  `area` int(6) DEFAULT NULL,
  `password` varchar(6) DEFAULT NULL,
  `username` varchar(6) DEFAULT NULL,
  `email` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`ID`, `emp_id`, `name`, `role`, `mobile_1`, `mobile_2`, `manager`, `team`, `committee`, `dept`, `zone`, `division`, `district`, `city`, `town`, `area`, `password`, `username`, `email`) VALUES
(3, '101', 'Ahsam Khan', 2, '300', '310', 60, 'kk', '12', 12, 12, 321, 3, 1, 1, 12, '123', 'abdul', 'tariq@gmail.com'),
(6, NULL, 'Ayaz Baig', 2, NULL, NULL, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(8, NULL, 'Dawar Khan', 2, NULL, NULL, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(9, NULL, 'Farrukh Ali', 2, NULL, NULL, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(10, NULL, 'Ghulam Rasool', 2, NULL, NULL, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(11, NULL, 'Haider Shah', 2, NULL, NULL, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(12, NULL, 'Hasan Khan', 2, NULL, NULL, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(13, NULL, 'Hassan Ishtiaq', 2, NULL, NULL, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(14, NULL, 'Imtiaz Hussain', 2, NULL, NULL, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(15, NULL, 'Jahanzeb Ali', 2, NULL, NULL, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(16, NULL, 'Jamal Ali', 2, NULL, NULL, 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(17, NULL, 'Jilann Khan', 2, NULL, NULL, 61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(18, NULL, 'Rahim Khan', 2, NULL, NULL, 61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(19, NULL, 'Rehman Khan', 2, NULL, NULL, 61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(20, NULL, 'Romi Khan', 2, NULL, NULL, 61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(21, NULL, 'Saim Ikram', 2, NULL, NULL, 61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(22, NULL, 'Saleem Khan', 2, NULL, NULL, 61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(23, NULL, 'Shah Ali', 2, NULL, NULL, 61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(24, NULL, 'Shaikh Hassan', 2, NULL, NULL, 61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(25, NULL, 'Shakoor Khan', 2, NULL, NULL, 61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(26, NULL, 'Wilayat Khan', 2, NULL, NULL, 61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(27, NULL, 'Zahid Ali', 2, NULL, NULL, 61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(28, NULL, 'Zohaib Khan', 2, NULL, NULL, 61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(29, NULL, 'Zohiar Aslam', 2, NULL, NULL, 61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(30, NULL, 'Bilal Akram', 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(31, NULL, 'Abdul Mohsin', 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(32, NULL, 'Ghulam Mustafa', 1, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(33, NULL, 'Wasim Ashiq', 1, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(34, NULL, 'Imran Qamar', 1, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(35, NULL, 'Imran Ali', 1, NULL, NULL, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(36, NULL, 'Rehman', 1, NULL, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(37, NULL, 'Hasnain Toqeer', 1, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(38, NULL, 'Abid Riaz', 1, NULL, NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(39, NULL, 'Zeeshan Haider', 1, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(40, NULL, 'Ali Adnan', 1, NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(41, NULL, 'Umer Riaz', 1, NULL, NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(42, NULL, 'Zubair Gill', 1, NULL, NULL, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(43, NULL, 'M.Atif', 1, NULL, NULL, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(44, NULL, 'M. Asif', 1, NULL, NULL, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(45, NULL, 'Usman Ahmad', 1, NULL, NULL, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(46, NULL, 'Waqas Riaz', 1, NULL, NULL, 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(47, NULL, 'Iftikhar Hussain', 1, NULL, NULL, 17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(48, NULL, 'Lazar Gill', 1, NULL, NULL, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(49, NULL, 'Imran Haider', 1, NULL, NULL, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(50, NULL, 'Waseem Bahadur', 1, NULL, NULL, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(51, NULL, 'Hameed Raza', 1, NULL, NULL, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(52, NULL, 'Ali Faheem', 1, NULL, NULL, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(53, NULL, 'Yasir Gulzar', 1, NULL, NULL, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(54, NULL, 'Abdullah', 1, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(55, NULL, 'M. Sultan', 1, NULL, NULL, 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(56, NULL, 'M. Tayyab', 1, NULL, NULL, 26, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(57, NULL, 'M. Amir Nisar', 1, NULL, NULL, 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(58, NULL, 'Shehzad Hussain', 1, NULL, NULL, 28, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(59, NULL, 'Zubair Asif', 1, NULL, NULL, 29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(60, NULL, 'ASM1khan', 3, NULL, NULL, 62, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(61, NULL, 'ASM2khan', 3, NULL, NULL, 62, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(62, NULL, 'DSM1khan', 4, NULL, NULL, 63, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(63, NULL, 'RSM1khan', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(64, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@'),
(65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'agent@');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `loc_pak_city`
--
ALTER TABLE `loc_pak_city`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `loc_pak_dist`
--
ALTER TABLE `loc_pak_dist`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `route`
--
ALTER TABLE `route`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `route_assignment`
--
ALTER TABLE `route_assignment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `shop_loc_web`
--
ALTER TABLE `shop_loc_web`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `shop_managers`
--
ALTER TABLE `shop_managers`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `shop_route`
--
ALTER TABLE `shop_route`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `shop_type`
--
ALTER TABLE `shop_type`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `shop_typewise`
--
ALTER TABLE `shop_typewise`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `loc_pak_city`
--
ALTER TABLE `loc_pak_city`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `loc_pak_dist`
--
ALTER TABLE `loc_pak_dist`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `route`
--
ALTER TABLE `route`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `route_assignment`
--
ALTER TABLE `route_assignment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `shop`
--
ALTER TABLE `shop`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;
--
-- AUTO_INCREMENT for table `shop_loc_web`
--
ALTER TABLE `shop_loc_web`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;
--
-- AUTO_INCREMENT for table `shop_managers`
--
ALTER TABLE `shop_managers`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;
--
-- AUTO_INCREMENT for table `shop_route`
--
ALTER TABLE `shop_route`
  MODIFY `job_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `shop_type`
--
ALTER TABLE `shop_type`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `shop_typewise`
--
ALTER TABLE `shop_typewise`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;
--
-- AUTO_INCREMENT for table `tbl_login`
--
ALTER TABLE `tbl_login`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
