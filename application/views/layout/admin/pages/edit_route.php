<style>
	section
{
	margin-top: 30px;
}
.myform
{
	margin-top: 40px;
}
.myform h3
{
	padding-left: 16px;
	padding-bottom: 15px;
}
</style>
<section>
<?= form_open('route_management/update_route'); ?>
	<div class="container">
	<div class="row myform">
		<h3>Edit Route</h3>
		<div class="col-md-4">
		<input type="text" name="get_id" class="form-control hide" value="<?= $single_route[0]->ID; ?>">
			<div class="form-group">
				<label for="">Route</label>
				<input type="text" name="route" class="form-control" value="<?= $single_route[0]->route; ?>">
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="">Frequency</label>
				<select name="frequency" class="chosen-select form-control">
					<option value="0" selected disabled>Select Option</option>
					<option value="1">01</option>
					<option value="2">02</option>
					<option value="3">03</option>
					<option value="4">04</option>
				</select>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-4 hide">
					<div class="form-group">
						<label for="">Preferred Supervisor</label>
						<input type="text" name="supervisor" value="2">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="">Preferred ASM</label>
						<select name="asm" class="chosen-select form-control">
							<option value="<?= $single_route[0]->asm; ?>" selected><?= $single_route[0]->asm; ?></option>
							<?php if(isset($all_asm)): ?>
								<?php foreach($all_asm as $asm): ?>
							<option value="<?= $asm->ID; ?>"><?= $asm->name; ?></option>
						<?php endforeach; ?>
					<?php endif; ?>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="">Preferred District</label>
						<select name="district" id="" class="chosen-select form-control">
							<option value="<?= $single_route[0]->district; ?>" selected><?= $single_route[0]->district; ?></option>
							<?php if(isset($all_dist)): ?>
								<?php foreach($all_dist as $dist): ?>
							<option value="<?= $dist->ID; ?>"><?= $dist->district; ?></option>
						<?php endforeach; ?>
					<?php endif; ?>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="">Preferred City</label>
						<select name="city" id="" class="chosen-select form-control">
							<option value="<?= $single_route[0]->city; ?>" selected><?= $single_route[0]->city; ?></option>
							<?php if(isset($all_cities)): ?>
								<?php foreach($all_cities as $city): ?>
							<option value="<?= $city->ID; ?>"><?= $city->city; ?></option>
						<?php endforeach; ?>
					<?php endif; ?>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="">Distance</label>
						<input type="text" name="distance" id="" class="form-control" value="<?= $single_route[0]->distance; ?>">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="">Keywords</label>
						<input type="text" name="keyword" id="" class="form-control" value="<?= $single_route[0]->keywords; ?>">
					</div>
				</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label for="">Description</label>
				<input type="text" name="desp" id="" class="form-control" value="<?= $single_route[0]->description; ?>">
			</div>
		</div>
	</div>
		<div class="row" style="margin-top: 20px;">
			<center>
			<a href="<?= base_url(); ?>route_management/view_route" class="btn btn-primary">Back</a>
				<input type="submit" value="Update" class="btn btn-success" name="update">
			</center>
		</div>
	</div>
	<?= form_close(); ?>
</section>