<section>
	<div class="container">
	<div class="row" style="margin-bottom: 40px;margin-top: 40px;">
		<div id="map" style="height: 350px;"></div>
	</div>
		<div class="row">
		<?php 
		// if($this->uri->segment(3) == "all")
		// {
		// 	$this->db->select("*");
		// 	$this->db->from('shop_route');
		// 	// $this->db->where('shop_route.route_id', $id);
		// 	$this->db->join('shop', 'shop.ID = shop_route.shop ','left');
		// 	$this->db->join('route', 'route.ID = shop_route.route_id ','left');
		// 	$query = $this->db->get();
		// 	$data = $query->result();
		// 	echo "<pre>";
		//     print_r($data);
		//     echo "</pre>";
		// }

		 ?>
		<?php $id = $this->uri->segment(3); ?>
		<?php 
		if($id == "all")
		{
		$this->db->select("*");
		$this->db->from('shop_route');
		// $this->db->where('shop_route.route_id', $id);
		$this->db->join('shop', 'shop.ID = shop_route.shop ','left');
		$this->db->join('route', 'route.ID = shop_route.route_id ','left');
		$query = $this->db->get();
		$data = $query->result();
		}
		else
		{
			$this->db->select("*");
			$this->db->from('shop_route');
			$this->db->where('shop_route.route_id', $id);
			$this->db->join('shop', 'shop.ID = shop_route.shop ','left');
			$this->db->join('route', 'route.ID = shop_route.route_id ','left');
			$query = $this->db->get();
			$data = $query->result();
		}
		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";
		

		 ?>
		 
			<table id="myTable">
				<thead>
					<th>ID</th>
					<th>Shops Name</th>
					<th>Assigned Route</th>
					<th>Rank</th>
					<th>Shop (Active/Deactive)</th>
					<th>Open Shop Data</th>
					<th>Map</th>
					<th>Info</th>
					<!-- <th>Contact</th> -->
					<th>Manage</th>
				</thead>
				<tbody>
				<?php if(isset($data)): ?>
				<?php foreach($data as $shop_route): ?>
					<tr>
						<td><?= $shop_route->job_id; ?></td>
						<td><?= $shop_route->shop_name; ?></td>
						<td><?= $shop_route->route; ?></td>
						<td>
							<select name="rank" data-id="<?= $shop_route->job_id; ?>"  id="" class="form-control assign_rank">
								<?php if($shop_route->rank_m == 0): ?>
									<option value="0" selected>0</option>
								<?php else: ?>
									<option value="0">0</option>
									<option value="<?= $shop_route->rank_m; ?>" selected ><?= $shop_route->rank_m; ?></option>									
								<?php endif; ?>
								<?php 
								$count_no = $this->uri->segment(3);
								// $count_no = 2;
								$this->db->where('route_id', $count_no);
								$query = $this->db->get('shop_route');
								$rowcount = $query->num_rows();

								$this->db->select("rank_m");
								$this->db->where('route_id', $shop_route->route_id);
								$this->db->where('rank_m  != ', 0, FALSE);
								$query1 = $this->db->get('shop_route');
								$jobRanks = $query1->result();
								$jr = [];
								foreach ($jobRanks as $key => $value) {
									array_push($jr, $value->rank_m);
								}
								print_r($jr);
						 		?>
								<?php for($i = 1; $i <= $rowcount; $i++): ?>
									<?php if(!in_array($i, $jr) AND $i != 0): ?>
									<option value="<?= $i; ?>" <?= ($i == $shop_route->rank_m) ? 'selected' : '' ?> ><?= $i; ?></option>
									<?php endif; ?>
								<?php endfor; ?>
							</select>
						</td>
						<td>
							<label class="radio-inline">
							<input type="checkbox" name="optradio_<?= $shop_route->job_id; ?>" value="1" class="optradio_btn" data-id="<?= $shop_route->job_id; ?>" <?= ($shop_route->rank_2 == 1) ? "checked" : ""  ?>> Active							
    						</label>
						</td>
						<td>Will be there</td>
						<td><a href="#">
							<h4 class="map_marker" 
								data-long="<?= $shop_route->long; ?>" 
								data-lat="<?= $shop_route->lat; ?>"
								data-name="<?= $shop_route->shop_name; ?>"
								>
								<i class="fa fa-map-marker"></i></h4></a></td>
						<td><a href="#" data-toggle="tooltip" title="This is demo, Information will be here"><h4><i class="fa fa-info"></i></h4></a></td>
						<!-- <td>Will be there</td> -->
						<td> <a href="<?= base_url(); ?>route_management/delete_route_shop/<?= $shop_route->job_id; ?>" class="btn btn-danger">X</a></td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>

				</tbody>
			</table>
		</div>
		
	</div>
</section>