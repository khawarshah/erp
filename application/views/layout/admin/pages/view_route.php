<section>
	<div class="container">
		<div class="row">
			<table id="myTable">
				<thead>
					<tr>
						<th>Route Name</th>
						<th>ASM</th>
						<th>District</th>
						<th>City</th>
						<th>Distance</th>
						<th>Map</th>
						<th>Manage</th>
					</tr>
				</thead>
				<tbody>
				<?php if(isset($all_route)): ?>
					<?php foreach($all_route as $routes): ?>
					<tr>
						<td><?= ucwords($routes->route); ?></td>
						<td><?= ucwords($routes->user_name); ?></td>
						<td><?= ucwords($routes->dist_name); ?></td>
						<td><?= ucwords($routes->city_name); ?></td>
						<td><?= ucwords($routes->distance); ?> km</td><!-- 
						data-toggle="modal" data-target="#myModal" -->
						<td><a href="#" class="show_shops" data-id="<?= $routes->route_id ?>">Map <i class="fa fa-map-marker"></i></a></td>
						<td><a href="<?= base_url(); ?>route_management/assign_route/<?= $routes->route_id ?>" class="btn btn-success">Add Shops</a> | <a href="<?= base_url(); ?>route_management/rank_route/<?= $routes->route_id ?>" class="btn btn-success">Shop Ranks</a> | <a href="<?= base_url(); ?>route_management/edit_route/<?= $routes->route_id ?>" class="btn btn-success"><i class="fa fa-edit"></i></a> | <a href="<?= base_url(); ?>route_management/delete/<?= $routes->route_id ?>" class="btn btn-danger">X</a></td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</section>
<section>
	<div class="container">
		<div class="row">
			
		</div>
	</div>
</section>

 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">All Shops in this route</h4>
        </div>
        <div class="modal-body">
          <div class="shop_locations_map" id="shop_locations_map" style="height: 400px; width: 100%;"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>