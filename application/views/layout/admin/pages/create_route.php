<style>
	section
{
	margin-top: 30px;
}
.myform
{
	margin-top: 40px;
}
.myform h3
{
	padding-left: 16px;
	padding-bottom: 15px;
}
</style>
<section>
<?= form_open('route_management/add_route'); ?>
	<div class="container">
	<div class="row myform">
		<h3>Create Route</h3>
		<div class="col-md-4">
			<div class="form-group">
				<label for="">Route</label>
				<input type="text" name="route" class="form-control" placeholder="Route Name" required>
			</div>
		</div>
		<div class="col-md-4">
			<div class="form-group">
				<label for="">Frequency</label>
				<select name="frequency" class="chosen-select form-control">
					<option value="0" selected disabled>Select Option</option>
					<option value="1">01</option>
					<option value="2">02</option>
					<option value="3">03</option>
					<option value="4">04</option>
				</select>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-4 hide">
					<div class="form-group">
						<label for="">Preferred Supervisor</label>
						<input type="text" name="supervisor" value="2">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="">Preferred ASM</label>
						<select name="asm" class="chosen-select form-control">
							<option value="0" selected disabled>Select Option</option>
							<?php if(isset($all_asm)): ?>
								<?php foreach($all_asm as $asm): ?>
							<option value="<?= $asm->ID; ?>"><?= $asm->name; ?></option>
						<?php endforeach; ?>
					<?php endif; ?>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="">Preferred District</label>
						<select name="district" id="" class="chosen-select form-control">
							<option value="0" selected disabled>Select Option</option>
							<?php if(isset($all_dist)): ?>
								<?php foreach($all_dist as $dist): ?>
							<option value="<?= $dist->ID; ?>"><?= $dist->district; ?></option>
						<?php endforeach; ?>
					<?php endif; ?>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="">Preferred City</label>
						<select name="city" id="" class="chosen-select form-control">
							<option value="0" selected disabled>Select Option</option>
							<?php if(isset($all_cities)): ?>
								<?php foreach($all_cities as $city): ?>
							<option value="<?= $city->ID; ?>"><?= $city->city; ?></option>
						<?php endforeach; ?>
					<?php endif; ?>
						</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="">Distance</label>
						<input type="text" name="distance" id="" class="form-control">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
						<label for="">Keywords</label>
						<input type="text" name="keyword" id="" class="form-control">
					</div>
				</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-4">
			<div class="form-group">
				<label for="">Description</label>
				<input type="text" name="desp" id="" class="form-control">
			</div>
		</div>
	</div>
		<div class="row" style="margin-top: 20px;">
			<center>
				<input type="reset" value="Cancel" class="btn btn-danger">
				<input type="submit" value="Create Route" class="btn btn-success" name="save">
			</center>
		</div>
	</div>
	<?= form_close(); ?>
</section>