<style>
	.row	
	{
		margin-top: 50px;
	}
</style>
<section>
	<div class="container">
		<div class="row">
		
		<br><br>
			<table class="table table-striped">
  <thead>
    <tr>
      <th>#</th>
      <th>Shop Name</th>
      <th>Shop Type</th>
      <th>Description</th>
      <th>District</th>
      <th>Market</th>
      <th>Map</th>
      <th>Manage</th>
      <th>Delete</th>
    </tr>
  </thead>
  <tbody class="filtered-data">

    <tr>
      <th scope="row"><input type="checkbox" name="" checked></th>
      <td><?= ucwords("Data"); ?></td>
      <td><?= ucwords("Data") ?></td>
      <td><?= ucwords("Data"); ?></td>
      <td><?= ucwords("Data"); ?></td>
      <td><?= ucwords("Data"); ?></td>
      <td><a href="#"  data-toggle="modal" data-target="#myModal">Map <i class="fa fa-map-marker"></i></a></td>
      <td>
        <select name="" id="" class="form-control">
          <option value="" selected disabled>Select Option</option>
          <option value="">Active</option>
          <option value="">Deactive</option>
        </select>
      </td>
      <td><?= ucwords("Delete"); ?></td>
    </tr>
  </tbody>
</table>

		</div>
	</div>
</section>

<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <div class="contact_map" id="contact_map">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6082.26016811261!2d67.0911885283525!3d24.98198049884745!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3eb340cd7c3516fd%3A0x12119a1490666909!2sKnitex+International+%2C+Wholesale+Knitted+Fabric+Supplier%2CApparel+%26+Home+Textile+Manufacturer+%26+Exporters!5e0!3m2!1sen!2s!4v1497080040278" width="570" height="360" frameborder="0" style="border:0" allowfullscreen></iframe>
                                 <noscript>
                                    <blockquote class='warning'>
                                       <p><b>JavaScript must be enabled in order for you to use Google Maps.</b> <br/>However, it seems JavaScript is either disabled or not supported by your browser. <br/>To view Google Maps, enable JavaScript by changing your browser options, and then try again.</p>
                                    </blockquote>
                                 </noscript>
                                 <div id='mapbody1_6wutm_0' style="display: none; text-align:center">
                                    <div id="googlemap1_6wutm_0" class="map"></div>
                                 </div>
                                 <script type='text/javascript'>/*<![CDATA[*/
                                    var mapconfig1_6wutm_0 = {"debug":"1","visualrefresh":"0","show":"1","mapclass":"","loadmootools":"0","timeinterval":"500","googlewebsite":"maps.google.com","align":"center","effect":"none","deflatitude":"40.579591","deflongitude":"-73.970668","centerlat":"","centerlon":"","address":"","latitudeid":"","latitudedesc":"1","latitudecoord":"0","latitudeform":"0","controltype":"UI","zoomtype":"None","svcontrol":"1","returncontrol":"1","zoom":"10","corzoom":"0","minzoom":"0","maxzoom":"19","rotation":"1","zoomnew":"0","zoomwheel":"1","keyboard":"0","maptype":"Normal","showmaptype":"1","shownormalmaptype":"1","showsatellitemaptype":"1","showhybridmaptype":"1","showterrainmaptype":"1","showearthmaptype":"1","showscale":"0","overview":"0","dragging":"1","marker":"1","traffic":"0","transit":"0","bicycle":"0","panoramio":"0","pano_userid":"","pano_tag":"","weather":"0","weathercloud":"0","weatherinfo":"1","weathertempunit":"celsius","weatherwindunit":"km","dir":"0","dirtype":"D","formdirtype":"0","avoidhighways":"0","avoidtoll":"0","diroptimize":"0","diralternatives":"0","showdir":"1","animdir":"0","animspeed":"1","animautostart":"0","animunit":"kilometers","formspeed":"0","formaddress":"0","formdir":"0","autocompl":"both","txtdir":"Directions: ","txtgetdir":"Get Directions","txtfrom":"From here","txtto":"To here","txtdiraddr":"Address: ","txt_driving":"","txt_avhighways":"","txt_avtoll":"","txt_walking":"","txt_bicycle":"","txt_transit":"","txt_optimize":"","txt_alternatives":"","dirdefault":"0","gotoaddr":"0","gotoaddrzoom":"0","txtaddr":"Address: ##","erraddr":"Address ## not found!","txtgotoaddr":"Goto","clientgeotype":"google","lightbox":"0","txtlightbox":"Open lightbox","lbxcaption":"","lbxwidth":"500px","lbxheight":"700px","lbxcenterlat":"","lbxcenterlon":"","lbxzoom":"","sv":"none","svwidth":"100%","svheight":"300px","svautorotate":"0","svaddress":"1","kmlrenderer":"google","kmlsidebar":"none","kmlsbwidth":"200px","kmllightbox":"0","kmlhighlite":"","proxy":"1","tilelayer":"","tilemethod":"","tileopacity":"1","tilebounds":"","tileminzoom":"0","tilemaxzoom":"19","twittername":"","twittertweets":"15","twittericon":"\/media\/plugin_googlemap2\/site\/Twitter\/twitter_map_icon.png","twitterline":"#ff0000ff","twitterlinewidth":"4","twitterstartloc":"0,0,0","lang":"en","mapType":"normal","geocoded":0,"tolat":"","tolon":"","toaddress":"","description":"","tooltip":"","kml":[],"kmlsb":[],"layer":[],"camera":[],"searchtext":"","latitude":"40.579591","longitude":"-73.970668","waypoints":[],"mapnm":"1_6wutm_0","descr":"0","geoxmloptions":{"titlestyle":" class=kmlinfoheader ","descstyle":" class=kmlinfodesc ","veryquiet":true,"quiet":true,"iwmethod":"click","sortbyname":null,"linktarget":"_self","linkmethod":"dblclick","lang":{"txtdir":"Directions: ","txtto":"To here","txtfrom":"From here","txtsrchnrby":"","txtzoomhere":"","txtaddrstart":"","txtgetdir":"","txtback":"","txtsearchnearby":"","txtsearch":""},"inputsize":"25"},"icontype":"","earthoptions":{"timeout":"100","borders":true,"buildings":false,"roads":false,"terrain":false,"lookat":[],"camera":[]}};
                                    var mapstyled1_6wutm_0 = null;
                                    var googlemap1_6wutm_0 = new GoogleMaps('1_6wutm_0', mapconfig1_6wutm_0, mapstyled1_6wutm_0);
                                    /*]]>*/
                                 </script>
                              </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>