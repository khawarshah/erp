<section>
	<div class="container">
		<div class="row">
			<table id="myTable">
				<thead>
					<tr>
						<th>Shop Name</th>
						<th>Type</th>
						<th>Market</th>
						<th>Icons</th>
						<th>Displays</th>
						<th>Add POP</th>
						<th>Map</th>
						<th>Info</th>
						<th>Report</th>
					</tr>
				</thead>
				<tbody>
				<?php if(isset($all_shops)): ?>
					<?php foreach($all_shops as $shops): ?>
					<tr>
						<td><?= ucwords($shops->shop_name); ?></td>
						<td>Type</td>
						<td>Market</td>
						<td>Icons</td>
						<td><a href="<?= base_url(); ?>shops/shop_display" class="btn btn-info">Add Displays</a></td>
						<td>Add POP</td>
						<td>Map</td>
						<td>Info</td>
						<td>Report</td>
					</tr>
				<?php endforeach; ?>
				<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>
</section>