<section>
	<div class="container">
		<div class="row"  style="margin-top: 30px;">
			<div class="col-md-4">
				<div class="form-group">
					<label for="">Display list</label>
					<select name="" id="" class="form-control">
						<option value="">Owned</option>
						<option value="">Not Owned</option>
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="">Category</label>
					<select name="" id="" class="form-control">
						<option value="">Owned</option>
						<option value="">Not Owned</option>
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="">Display</label>
					<select name="" id="" class="form-control">
						<option value="">Owned</option>
						<option value="">Not Owned</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="pull-right">
				<input type="submit" value="Add Display" class="btn btn-success">
			</div>
		</div>
		<div class="row" style="margin-top: 50px;">
			<table id="myTable">
				<thead>
					<tr>
						<th>Display Shop ID</th>
						<th>Display Name</th>
						<th>Description</th>
						<th>Temp Owned</th>
						<th>Planogram</th>
						<th>Get Barcode</th>
						<th>Code</th>
						<th>SKU</th>
						<th>Info Report</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</section>