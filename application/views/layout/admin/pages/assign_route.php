<style>
	.row	
	{
		margin-top: 50px;
	}
</style>
<?= form_open('route_assignment/assign_route', 'class="filer_s"'); ?>
<section>
	<div class="container">
	<div class="row">
		<div class="col-md-4">
			<label for="">Route Name :</label>
			<input type="text" value="<?= $get_route_name[0]->route; ?>" class="form-control" disabled>
		</div>
		<input type="text" id="route_id" name="route_id" value="<?= $route_id; ?>" class="hide">
	</div>
		<div class="row">
			<div class="col-md-4">
			<label for="">Filter By Shop Type</label>
				<select multiple="multiple" class="form-control btn-filter-shop shop_type" style="height: 160px;">
				<?php if(isset($all_types)): ?>
					<?php foreach($all_types as $all_type): ?>
						<option value="<?= $all_type->ID; ?>"><?= ucwords($all_type->shop_type); ?></option>
					<?php endforeach; ?>
				<?php endif; ?>
				</select>
			</div>
			<div class="col-md-4">
			<label for="">Filter By District</label>
				<select multiple="multiple" class="form-control btn-filter-shop districts district-class" style="height: 160px;">
				<?php if(isset($districts)): ?>
					<?php foreach($districts as $district): ?>
						<option value="<?= $district->ID; ?>"><?= ucwords($district->district); ?></option>
					<?php endforeach; ?>
				<?php endif; ?>
				</select>
			</div>
			<div class="col-md-4">
			<label for="">Filter By Market</label>
				<select multiple="multiple" class="form-control btn-filter-shop blocks related-block" style="height: 160px;">
				<?php if(isset($all_markets)): ?>
					<?php foreach($all_markets as $all_market): ?>
						<option value="<?= $all_market->block; ?>"><?= ucwords($all_market->block); ?></option>
					<?php endforeach; ?>
				<?php endif; ?>
				</select>
			</div>

		</div>
		<div class="row">
		<label for="superviser">&nbsp;My Shops (Own created Shops)</label>		
		<input type="checkbox" name="superviser" id="superviser" class="pull-left own-shops btn-filter-shop">
		<button class="btn btn-success pull-right btn-rank">Shops Rank</button>
		<br><br>
			<table class="table table-striped"  style="padding-top: 20px;">
  <thead>
    <tr>
      <th>#</th>
      <th>Shop Name</th>
      <th class="type-visibility" >Shop Type</th>
      <th >Description</th>
      <th class="districts-visibility">District</th>
      <th class="market-visibility">Market</th>
    </tr>
  </thead>
  <tbody class="filtered-data">
  <?php if(isset($all_shops)): ?>
  	<?php foreach($all_shops as $all_shop): ?>
    <tr>
      <th scope="row"><input type="checkbox" checked class="checkbox_id" name="selector[]" value="<?= $all_shop->as_shop_id; ?>"></th>
      <td><?= ucwords($all_shop->as_shop_name); ?></td>
      <td><?= ucwords($all_shop->as_shop_type) ?></td>
      <td><?= ucwords($all_shop->as_shop_description); ?></td>
      <td><?= ucwords($all_shop->as_shop_district); ?></td>
      <td><?= ucwords($all_shop->as_shop_block); ?></td>
    </tr>
	<?php endforeach; ?>
   <?php endif; ?>
  </tbody>
</table>
<button class="btn btn-success pull-right btn-rank">Shops Rank</button>
		</div>
	</div>
</section>
<?= form_close(); ?>