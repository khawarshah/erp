<style>
	.btn-own
	{
		padding: 10px 10px;
		padding: 7px 38px
	}
	.btn-own-2
	{
		margin-top: 20px;
	}
	table th
	{
		padding: 8px 55px;
	}
	table td
	{
		text-align: center;
		padding: 17px;
	}
	table tr
	{
		
	}
</style>
<form class="form-assign-route">
	<section>
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="form-group">
						<div class="row">
						<div class="col-md-2">
							<label for="">Merchandiser</label>
						</div>
						<div class="col-md-10">
							<select name="marchandiser" id="marchandiser"  class="form-control marchandiser">
								<?php foreach ($merchandisers as $key => $value): ?>
									<option value="<?= $value->ID; ?>"><?= $value->name; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					</div>
					<div class="form-group">
						<div class="row">
						<div class="col-md-2">
							<label for="">Route</label>
						</div>
						<div class="col-md-10">
							<select name="route" id="route" class="form-control route">
								<?php foreach ($routes as $key => $value): ?>
									<option value="<?= $value->ID; ?>"><?= $value->route; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					</div>
					<div class="form-group">
						<div class="row">
							<div class="pull-right" style="margin-right: 13px;">
								<label for="Weekly">Weekly</label>
								<input type="radio" id="Weekly" value="Weekly" name="repetation" class="btn btn-own">
								<label for="Monthly">Monthly</label>
								<input type="radio" id="Monthly" value="Monthly" name="repetation" class="btn btn-own">
								<label for="Yearly">Yearly</label>
								<input type="radio" id="Yearly" value="Yearly" name="repetation" class="btn btn-own">
								<label for="Period">Period</label>
								<input type="radio" id="Period" value="Period" name="repetation" class="btn btn-own">
								<label for="Custom">Custom Edit</label>
								<input type="radio" id="Custom" value="Custom Edit" name="repetation" class="btn btn-own">
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-2">
					<img src="<?= base_url(); ?>assets/images/profile-2.png" alt="" style="width: 84%;">
				</div>
				<div class="col-md-2">
					<p>ID : 219</p>
					<p>Sales Division Karachi Central</p>
					<button class="btn btn-own-2">Reports</button>
				</div>
			</div>
			<hr>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="row">
				<table>
					<thead>
						<th></th>
						<th>Mon</th>
						<th>Tue</th>
						<th>Wed</th>
						<th>Thu</th>
						<th>Fri</th>
						<th>Sat</th>
						<th>Sun</th>
						<th></th>
					</thead>
					<tbody>
						<tr>
							<td><strong>Week 1</strong></td>
							<td><input type="checkbox" name="selected_day[]" value="1-1"></td>
							<td><input type="checkbox" name="selected_day[]" value="1-2"></td>
							<td><input type="checkbox" name="selected_day[]" value="1-3"></td>
							<td><input type="checkbox" name="selected_day[]" value="1-4"></td>
							<td><input type="checkbox" name="selected_day[]" value="1-5"></td>
							<td><input type="checkbox" name="selected_day[]" value="1-6"></td>
							<td><input type="checkbox" name="selected_day[]" value="1-7"></td>
							<td><input type="submit" class="week_1" class="btn"></td>
						</tr>
						<tr>
							<td><strong>Week 2</strong></td>
							<td><input type="checkbox" name="selected_day[]" value="2-1"></td>
							<td><input type="checkbox" name="selected_day[]" value="2-2"></td>
							<td><input type="checkbox" name="selected_day[]" value="2-3"></td>
							<td><input type="checkbox" name="selected_day[]" value="2-4"></td>
							<td><input type="checkbox" name="selected_day[]" value="2-5"></td>
							<td><input type="checkbox" name="selected_day[]" value="2-6"></td>
							<td><input type="checkbox" name="selected_day[]" value="2-7"></td>
							<td><input type="submit" class="week_2" class="btn"></td>
						</tr>
						<tr>
							<td><strong>Week 3</strong></td>
							<td><input type="checkbox" name="selected_day[]" value="3-1"></td>
							<td><input type="checkbox" name="selected_day[]" value="3-2"></td>
							<td><input type="checkbox" name="selected_day[]" value="3-3"></td>
							<td><input type="checkbox" name="selected_day[]" value="3-4"></td>
							<td><input type="checkbox" name="selected_day[]" value="3-5"></td>
							<td><input type="checkbox" name="selected_day[]" value="3-6"></td>
							<td><input type="checkbox" name="selected_day[]" value="3-7"></td>
							<td><input type="submit" class="week_3" class="btn"></td>
						</tr>
						<tr>
							<td><strong>Week 4</strong></td>
							<td><input type="checkbox" name="selected_day[]" value="4-1"></td>
							<td><input type="checkbox" name="selected_day[]" value="4-2"></td>
							<td><input type="checkbox" name="selected_day[]" value="4-3"></td>
							<td><input type="checkbox" name="selected_day[]" value="4-4"></td>
							<td><input type="checkbox" name="selected_day[]" value="4-5"></td>
							<td><input type="checkbox" name="selected_day[]" value="4-6"></td>
							<td><input type="checkbox" name="selected_day[]" value="4-7"></td>
							<td><input type="submit" class="week_4" class="btn"></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label for="">Starting Date</label>
						<input type="date" name="start_date"  class="form-control start_date">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="">Closing Date</label>
						<input type="date" name="end_date"  class="form-control end_date">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="pull-right">
					<input type="submit" id="assign_route" value="Repeat Schedule" class="btn btn-own" style="margin-right: 24px;">
				</div>
			</div>
		</div>
	</section>
</form>