     <script type="text/javascript">
     	function base_url(){
     		return "<?= base_url() ?>";
     	}
     </script>
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>assets/js/shop-filter.js"></script>
      <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
      <script>
        $(document).ready(function(){
          $('#myTable').DataTable();
        });

        jQuery(document).on("click", "#assign_route", function(event){
            event.preventDefault();
            var fomData = jQuery(".form-assign-route").serializeArray();
            jQuery.get('<?= base_url() ?>route_assignment/route_assignmen', fomData, function(data, textStatus, xhr) {
                // console.log(data);
            });
            window.location.replace("<?= base_url(); ?>route_assignment/route_calenderlist");
            // console.log(fomData);

        });

        </script>
     <script>
      $(document).ready(function(){
        $(".btn-rank").click(function(event){
          event.preventDefault();
          var shop_id = $("input[name='checkbox_id']:checked").val();
          var route_id = $("#route_id").val();
          var val = [];
          var formdata = jQuery(".filer_s").serializeArray();
          // console.log(data);
          if($('.checkbox_id').is(':checked'))
          {
        $(':checkbox:checked').each(function(i){
          val[i] = $(this).val();
          // console.log(val[i]);
        });
            jQuery.get("<?= base_url(); ?>route_management/shops_route", formdata, function(data){
            // console.log(data);

        });
             window.location = '<?= base_url(); ?>route_management/rank_route/' + route_id;
        //     window.location.href = "<?= base_url(); ?>route_management/rank_route";
          }
          else
          {
            alert("Please Select Atleast One Shop For Route");
          }
        });
      });
      </script>
      <script>
        $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
        });
      </script>
      <script>
      jQuery(document).on("change", ".district-class", function(){
        var districts = jQuery(this).val();
        var h = "";
        jQuery.get("<?= base_url(); ?>route_management/shops_dist_filter", {
            districts : districts
        }, function(data){
            // console.log(data);
            jQuery.each(jQuery.parseJSON(data), function(ind, val){
                h += "<option value='"+val.block+"'>"+val.block+"</option>";
            });
            jQuery(".related-block").html(h);
        });
      });

  jQuery(document).on("change", ".optradio_btn", function(){
    if (jQuery(this).is(':checked')) {
      var value = jQuery(this).val();
    } else{
      var value = 0;
    }
    var id = jQuery(this).data("id");
    // console.log(id +"--"+ value);
    jQuery.get("<?= base_url(); ?>route_management/update_status",{
      id : id,
      value : value
    }, function(){
      // location.reload();
    });
  }); 


  jQuery(document).on("change", ".assign_rank", function(){
    var id = jQuery(this).data("id");
    var value = jQuery(this).val();
    // console.log(id +"--"+ value);
    jQuery.get("<?= base_url(); ?>route_management/update_rank",{
      id : id,
      value : value
    }, function(){
      location.reload();
    });
  });
      </script>
      <script>
      if (top.location.pathname === 'route_management/rank_route')
      {
      $( window ).load(function() {
        alert("Ok");
      });
      }
      </script>
       <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDVjqLuhwzmEU742UOEp8MddWY8ApUDbmM"></script>
      <script>
              var map;
      function initialize(lat = null, lng = null, address = "No Shop Selected") {
        if(lat == null && lng == null){
            lat = -34.397;
            lng = 150.644;
        }

        var mapOptions = {
          zoom: 15,
          center: {lat: lat, lng: lng}
        };
        map = new google.maps.Map(document.getElementById('map'),
            mapOptions);

        var marker = new google.maps.Marker({
          // The below line is equivalent to writing:
          // position: new google.maps.LatLng(lat, lng)
          position: {lat: lat, lng: lng},
          map: map
        });

        // You can use a LatLng literal in place of a google.maps.LatLng object when
        // creating the Marker object. Once the Marker object is instantiated, its
        // position will be available as a google.maps.LatLng object. In this case,
        // we retrieve the marker's position using the
        // google.maps.LatLng.getPosition() method.
        var infowindow = new google.maps.InfoWindow({
          content: address
        });
        infowindow.open(map, marker);
      }


      google.maps.event.addDomListener(window, 'load', initialize(-34.397, 150.644));

        // jQuery(document).on("click", ".map_marker", function(e){
        //     e.preventDefault();
        //     var lat = jQuery(this).data("lat");
        //     var long = jQuery(this).data("long");
        //     var name = "Shop name: <strong>" + jQuery(this).data("name") + "</strong>";
        //     initialize(lat, long, name);
        // });

         jQuery(document).on("click", ".map_marker", function(e){
            e.preventDefault();
            var lat = jQuery(this).data("lat");
            var long = jQuery(this).data("long");
            var name = "Shop name: <strong>" + jQuery(this).data("name") + "</strong>";
            initialize(lat, long, name);
        });

    </script>
    <script type="text/javascript">
          var marker = null, i;

      jQuery(document).on("click", ".show_shops", function(e){
          e.preventDefault();
          var route = jQuery(this).data("id");
          jQuery.get("<?= base_url(); ?>route_management/shop_locations/"+route, function(data){
               map1 = null;

                    multiple_shops(data);
                    jQuery('#myModal').modal('show');
          });
          
          setTimeout(function(){
            jQuery(window).resize();
          google.maps.event.trigger(jQuery('#shop_locations_map'), 'resize');

          }, 5000);
      });
      var map1 = null;
     function multiple_shops(locations) {
          locations = jQuery.parseJSON(locations);


          // map1 = new google.maps.Map(document.getElementById('shop_locations_map'), {
          //      zoom:10,
          //      center: new google.maps.LatLng(24.8615, 67.0099),
          //      mapTypeId: google.maps.MapTypeId.ROADMAP
          // });
          var mapOptions = {
               zoom: 10,
               center: {lat: 24.8615, lng: 67.0099}
          };

          if(map1 == null){
          map1 = new google.maps.Map(document.getElementById('shop_locations_map'),
            mapOptions);          
          }
          google.maps.event.trigger(map1, 'resize');

          if(marker != null){
               for (var i = 0; i < marker.length; i++) {
                    marker[i].setMap(map);
               }
          }          

          for (i in locations) {  
               if(locations[i].slat != "" && locations[i].slng != "" ){
                    var infowindow1 = new google.maps.InfoWindow();
                    marker = new google.maps.Marker({
                       position: new google.maps.LatLng(parseFloat(locations[i].slat), parseFloat(locations[i].slng)),
                       map: map1,
                       animation: google.maps.Animation.DROP,
                    });

                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                       return function() {
                         infowindow1.setContent(locations[i].sname);
                         infowindow1.open(map1, marker);
                       }
                    })(marker, i));
               }


          }
          $(window).on('resize',function(){
              google.maps.event.trigger(map1, 'resize');
              map1.panTo(marker.getPosition());
          });
     }
    </script>
   </body>
</html>
