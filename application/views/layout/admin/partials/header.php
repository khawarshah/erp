<!DOCTYPE html>

<html lang="en">

   <head>

      <meta charset="utf-8">

      <meta http-equiv="X-UA-Compatible" content="IE=edge">

      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

      <title>Form</title>

      <link rel="stylesheet" href="" type="<?= base_url(); ?>/assets/css/style.css">

      <!-- Bootstrap -->

      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

      <!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>

      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

      <![endif]-->

   </head>

   <body>

   <nav class="navbar navbar-default">

  <div class="container-fluid">

    <div class="navbar-header">

      <a class="navbar-brand" href="<?= base_url(); ?>home">Proton 360 Admin panel</a>

    </div>

    <ul class="nav navbar-nav">

      <li class="active"><a href="<?= base_url(); ?>home">Home</a></li>

      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Route<span class="caret"></span></a>

        <ul class="dropdown-menu">

          <li><a href="<?= base_url(); ?>route_management/create_route">Create Route</a></li>

          <li><a href="<?= base_url(); ?>route_management/view_route">View Route</a></li>

          <!-- <li><a href="<?= base_url(); ?>route_management/rank_route/all">View Ranks</a></li> -->

          <li><a href="<?= base_url(); ?>route_assignment">Route Assignment</a></li>
          <li><a href="<?= base_url(); ?>route_assignment/route_calenderlist">View Route Assignment</a></li>

        </ul>

      </li>

      <li><a href="<?= base_url(); ?>shops">Shop</a></li>

      <li><a href="<?= base_url(); ?>asset">SKU</a></li>

      <li><a href="<?= base_url(); ?>asset">Planogram</a></li>

      <li><a href="<?= base_url(); ?>asset">Assets</a></li>

     <!--  <li><a href="#">Page 2</a></li> -->

    </ul>

    <ul class="nav navbar-nav navbar-right">

      <li><a href="<?= base_url(); ?>home/logout"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>

    </ul>

  </div>

</nav>