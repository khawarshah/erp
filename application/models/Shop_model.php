<?php 

/**
* 
*/
class Shop_model extends CI_Model
{
	
	public function shops(){
		$this->db->select("
			shop_type.shop_type as as_shop_type,
			shop.ID as as_shop_id,
			shop.shop_name as as_shop_name,
			shop.description as as_shop_description,
			shop_loc_web.block as as_shop_block,
			loc_pak_dist.district as as_shop_district			
		");
		$this->db->from("shop");
		$this->db->join("shop_typewise", "shop_typewise.shop = shop.ID", "left");
		$this->db->join("shop_type", "shop_type.ID = shop_typewise.type", "left");
		$this->db->join("shop_loc_web", "shop_loc_web.shop_id = shop.ID", "left");
		$this->db->join("loc_pak_dist", "loc_pak_dist.ID = shop_loc_web.district", "left");
		return $this->db->get()->result();

	}


	public function filter_shop($shop_types = [], $districts = [], $locations = [], $supervisor){

		$this->db->select("
			shop_type.shop_type as as_shop_type,
			shop.ID as as_shop_id,
			shop.shop_name as as_shop_name,
			shop.description as as_shop_description,
			shop_loc_web.block as as_shop_block,
			loc_pak_dist.district as as_shop_district			
		");
		$this->db->from("shop");
		$this->db->join("shop_typewise", "shop_typewise.shop = shop.ID", "left");
		$this->db->join("shop_type", "shop_typewise.type = shop_type.ID", "left");
		$this->db->join("shop_loc_web", "shop_loc_web.shop_id = shop.ID", "left");
		$this->db->join("loc_pak_dist", "loc_pak_dist.ID = shop_loc_web.district", "left");
		$this->db->join("shop_managers", "shop_managers.shop = shop.ID", "left");

		if($shop_types[0] != "null"){
			$this->db->where_in("shop_typewise.type", $shop_types);
		}	
		if ($districts[0] != "null") {
			$this->db->where_in("shop_loc_web.district", $districts);
		}				
		if ($locations[0] != "null") {
			$this->db->where_in("shop_loc_web.block", $locations);
		}
		if ($supervisor != "false") {
			$this->db->where('shop_managers.supervisor',  "1");	
		}

		return $this->db->get()->result();
	}
	
		function shop_locations($id)
	{
		$this->db->select("shop.ID as sid, shop.lat as slat, shop.long as slng, shop.shop_name as sname");
		$this->db->from("shop_route");
		$this->db->join("shop", "shop.id = shop_route.shop","left");
		$this->db->where("shop_route.route_id", $id);
		return $this->db->get()->result();
	}
}