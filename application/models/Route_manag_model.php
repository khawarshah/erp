<?php 

class Route_manag_model extends CI_Model
{
	public function insert_route($data)
	{
		$this->db->insert('route', $data);
	}

	// For filters (Start Here)

	public function get_shoptype()
	{
		$query = $this->db->get('shop_type');
		return $query->result();
	}

	public function get_asms()
	{
		$query = $this->db->get('shop_managers');
		// $this->db->join('user', 'user.ID = shop_managers.asm','left');
		return $query->result();
	}
	public function get_district(){
		$districts = $this->db->get("loc_pak_dist");
		return $districts->result();
	}

	public function get_market()
	{
		$this->db->select('block');
		$this->db->group_by('block');
		$query = $this->db->get('shop_loc_web');
		return $query->result();
	}

	// For Filter (Close Here)

	public function get_shops()
	{
		$query = $this->db->get('shop');
		return $query->result();
	}

	// Todays work
	public function asm_users()
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('role',3);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_dist()
	{
		$query = $this->db->get('loc_pak_dist');
		return $query->result();
	}

	public function get_city()
	{
		$query = $this->db->get('loc_pak_city');
		return $query->result();
	}

	public function get_route()
	{
		$this->db->select("*, route.ID as route_id");
		$this->db->select("loc_pak_dist.district as dist_name, loc_pak_city.city as city_name, user.name as user_name");
		$this->db->from('route');
		$this->db->join('loc_pak_dist', 'loc_pak_dist.ID = route.district ','left');
		$this->db->join('loc_pak_city', 'loc_pak_city.ID = route.city ','left');
		$this->db->join('user', 'user.ID = route.asm','left');
		$query = $this->db->get();
		return $query->result();
	}

	public function route_edit($id)
	{
		$this->db->where('ID', $id);
		$query = $this->db->get('route');
		return $query->result();
	}

	public function update_route($id, $data)
	{
		$this->db->where('ID', $id);
		$this->db->update('route', $data);
	}

	public function delete_route()
	{
		$this->db->where('ID', $this->uri->segment(3));
		$this->db->delete('route');
	}

	public function insert_shop_into_route($data)
	{
		$this->db->insert('shop_route', $data);
	}

	public function shop_route()
	{
		$this->db->select("*");
		$this->db->from('shop_route');
		$this->db->join('shop', 'shop.ID = shop_route.shop ','left');
		$this->db->join('route', 'route.ID = shop_route.route_id ','left');
		$query = $this->db->get();
		return $query->result();
	}

	public function single_shop_route($id)
	{
		$this->db->where('job_id', $id);
		$query = $this->db->get('shop_route');
		return $query->result();
	}

	public function single_shop_update($id, $data)
	{
		$this->db->where('job_id', $id);
		$this->db->update('shop_route', $data);
	}

	public function delete_shop_route()
	{
		$this->db->where('job_id', $this->uri->segment(3));
		$this->db->delete('shop_route');
	}

	public function get_route_name($id)
	{
		$this->db->where('ID', $id);
		$query = $this->db->get('route');
		return $query->result();
	}

	public function get_merchandiser()
	{
		$this->db->where('role', 1);
		$query = $this->db->get('user');
		return $query->result();
	}

	// public function get_merchandiser()
	// {
	// 	$this->db->where('role', 1);
	// 	$query = $this->db->get('user');
	// 	return $query->result();
	// }

	public function route_assignment()
	{
		$query = $this->db->get('route');
		return $query->result();
	}

	public function dist_shops_filter($districts)
	{
		$this->db->select("*");
		// $this->db->distinct('block');
		$this->db->from('shop_loc_web');
		$this->db->where_in('district', $districts);
		$this->db->group_by('block');
		$query = $this->db->get();
		return $query->result();
	}

	public function status_update($id, $data)
	{
		$this->db->where('job_id', $id);
		$this->db->update('shop_route', $data);
	}

	public function rank_update($id, $data)
	{
		$this->db->where('job_id', $id);
		$this->db->update('shop_route', $data);
	}

	public function merchandisers(){
		$this->db->where('role', 1);
		$query = $this->db->get('user');
		return $query->result();
	}

	public function route_list(){
		$query = $this->db->get('route');
		return $query->result();		
	}

	public function route_assign_list(){
		$query = $this->db->get('route_assignment');
		return $query->result();		
	}	
}

 ?>