<?php 

class Site extends CI_Controller
{
	public function index()
	{
		$this->load->view('layout/admin/pages/login');
	}

	public function validate_credentails()
	{
		$query = $this->admin_login->validate();
		if($query)
		{
			$data = array(
						  'email' => $this->input->post('email'),
						  'is_login_in' => true,
						   );
			$this->session->set_userdata($data);
			redirect('home');
		}
		else
		{
			$data['error'] = "Wrong email or password";
			$this->load->view('layout/admin/pages/login', $data);
		}
	}
}

 ?>