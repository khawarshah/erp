<?php 

class Route_management extends CI_Controller
{
	function __construct()
	{
		Parent::__construct();
		$this->load->model('route_manag_model');
		$this->load->model('Shop_model');
	}
	public function index()
	{

	}

	public function create_route()
	{
		$asm = $this->route_manag_model->asm_users();
		$data['all_asm'] = $asm;
		$dist = $this->route_manag_model->get_dist();
		$data['all_dist'] = $dist;
		$city = $this->route_manag_model->get_city();
		$data['all_cities'] = $city;
		$data['main_content'] = 'layout/admin/pages/create_route';
		$this->load->view('admin_template', $data);
	}

	public function view_route()
	{
		$query = $this->route_manag_model->get_route();
		$data['all_route'] = $query;
		$data['main_content'] = 'layout/admin/pages/view_route';
		$this->load->view('admin_template', $data);
	}

	public function edit_route($id)
	{
		$query = $this->route_manag_model->route_edit($id);
		$data['single_route'] = $query;
		$asm = $this->route_manag_model->asm_users();
		$data['all_asm'] = $asm;
		$dist = $this->route_manag_model->get_dist();
		$data['all_dist'] = $dist;
		$city = $this->route_manag_model->get_city();
		$data['all_cities'] = $city;
		$data['main_content'] = 'layout/admin/pages/edit_route';
		$this->load->view('admin_template', $data);
	}

	public function update_route()
	{
		$id = $this->input->post('get_id');
		$data = array(
						'route'       => $this->input->post('route'),
						'supervisor'  => $this->input->post('supervisor'),
						'asm'         => $this->input->post('asm'),
						'district'    => $this->input->post('district'),
						'city'        => $this->input->post('city'),
						'distance'    => $this->input->post('distance'),
						'keywords'    => $this->input->post('keyword'),
						'description' => $this->input->post('desp')

					   );
		$this->route_manag_model->update_route($id,$data);
		redirect("route_management/view_route");
	}

	public function delete()
	{
		$this->route_manag_model->delete_route();
		redirect("route_management/view_route");
	}

	public function assign_route($id)
	{
		$route_name = $this->route_manag_model->get_route_name($id);
		$data['get_route_name'] = $route_name;
		$data['route_id'] = $id;
		// $query = $this->route_manag_model->get_route();
		// $data['all_route'] = $query;
		$type = $this->route_manag_model->get_shoptype();
		$data['all_types'] = $type;
		$districts = $this->route_manag_model->get_district();
		$data['districts'] = $districts;
		$market = $this->route_manag_model->get_market();
		$data['all_markets'] = $market;
		$shops = $this->Shop_model->shops();
		$data['all_shops'] = $shops;


		$data['main_content'] = 'layout/admin/pages/assign_route';
		$this->load->view('admin_template', $data);
	}

	public function assign_shops()
	{
		$data['main_content'] = 'layout/admin/pages/assign_shops';
		$this->load->view('admin_template', $data);
	}

	public function add_route()
	{
		if($this->input->post('save'))
		{
			$data = array(
						'route'       => $this->input->post('route'),
						'supervisor'  => $this->input->post('supervisor'),
						'asm'         => $this->input->post('asm'),
						'district'    => $this->input->post('district'),
						'city'        => $this->input->post('city'),
						'distance'    => $this->input->post('distance'),
						'keywords'    => $this->input->post('keyword'),
						'description' => $this->input->post('desp')

					   );
		$this->route_manag_model->insert_route($data);
		redirect('route_management/view_route');
		}
		
	}

	public function rank_route()
	{
		$shop_route = $this->route_manag_model->shop_route();
		$data['all_shop_route'] = $shop_route;
		$data['main_content'] = 'layout/admin/pages/rank_route';
		$this->load->view('admin_template', $data);
	}

	public function shops_route()
	{
		$shops  = $_GET['selector'];	
		$route_id = $_GET['route_id'];
		foreach ($shops as  $shop) {
		$data = [
					'shop'     => $shop,
					'route_id' => $route_id,
					'rank_m'   => 0,
					'rank_2'   => 1
				];
			$this->route_manag_model->insert_shop_into_route($data);			
		}
		// redirect('abc');

	}

	// public function set_rank($id)
	// {
	// 	$single_shop_route = $this->route_manag_model->single_shop_route($id);
	// 	$data['single_shop_date'] = $single_shop_route;
	// 	$data['main_content'] = 'layout/admin/pages/set_rank';
	// 	$this->load->view('admin_template', $data);
	// }

	// public function update_set_rank()
	// {
	// 		if($this->input->post('status') == 2)
	// 		{
	// 			$get_id = $this->input->post('id');
	// 			$data = array(
	// 				 'rank_m' => 99,
	// 				 'rank_2' => $this->input->post('status')

	// 				  );
	// 		$this->route_manag_model->single_shop_update($get_id, $data);
	// 		redirect('route_management/rank_route');
	// 		}
	// 		else
	// 		{
	// 			$get_id = $this->input->post('id');
	// 			$data = array(
	// 				 'rank_m' => $this->input->post('rank'),
	// 				 'rank_2' => $this->input->post('status')

	// 				  );
	// 		$this->route_manag_model->single_shop_update($get_id, $data);
	// 		redirect('route_management/rank_route');
	// 		}
	// }

	public function delete_route_shop()
	{	
		
		$this->route_manag_model->delete_shop_route();
		redirect('route_management/rank_route');
	}

	public function shops_dist_filter()
	{
		$districts = $_GET['districts'];
		$data = $this->route_manag_model->dist_shops_filter($districts);
		echo json_encode($data);
	}

	public function update_status()
	{
		$id = $_GET['id'];
		$value = $_GET['value'];

		$data = array(
					 'rank_2' => $value
					  );
		$this->route_manag_model->status_update($id, $data);
	}

	public function update_rank()
	{
		$id = $_GET['id'];
		$value = $_GET['value'];

		$data = array(
					 'rank_m' => $value
					  );
		$this->route_manag_model->rank_update($id, $data);
	}
	
	public function shop_locations($id){
		$shops = $this->Shop_model->shop_locations($id);
		echo json_encode($shops);
	}
	
}


 ?>