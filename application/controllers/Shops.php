<?php 

class Shops extends CI_Controller
{
	function __construct()
	{
		Parent::__construct();
		$this->load->model('shop_manag_model');
	}
	public function index()
	{
		$get_shops = $this->shop_manag_model->get_shops();
		$data['all_shops'] = $get_shops;
		$data['main_content'] = 'layout/admin/pages/shops';
		$this->load->view('admin_template', $data);
	}

	public function shop_display()
	{
		$data['main_content'] = 'layout/admin/pages/shop_display';
		$this->load->view('admin_template', $data);
	}
}

 ?>