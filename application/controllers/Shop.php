<?php
/**
* 
*/
class Shop extends CI_COntroller
{

	function __construct(){
		parent::__construct();
		$this->load->model("Shop_model");
	}

	public function filter($shop_types = "null", $districts = "null", $locations = "null", $supervisor = "null"){
		$shop_types = explode(",", $shop_types);
		$districts = explode(",", $districts);
		$locations = explode(",", utf8_decode(urldecode($locations)));
		// echo "<pre>";
		echo json_encode($this->Shop_model->filter_shop($shop_types, $districts, $locations, $supervisor));
	}
}