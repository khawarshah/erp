<?php 

class Route_assignment extends CI_Controller
{
	public function index()
	{
		$this->load->model('route_manag_model');
		$data['main_content'] = 'layout/admin/pages/route_assignment';
		$data['merchandisers'] = $this->route_manag_model->merchandisers();
		$data['routes'] = $this->route_manag_model->route_list();
		$this->load->view('admin_template', $data);
	}

	public function assign_route()
	{
		$get = $this->input->post('checkbox');
		var_dump($get);
	}

	public function route_assignmen(){
		
		foreach ($_GET['selected_day'] as $value) {
			$days = explode("-", $value);
			$data = [
				'merchandiser_id' => $_GET['marchandiser'],
				'route_id' => $_GET['route'],
				'week' => $days[0],
				'day' => $days[1],
				'routine_repeat' => $_GET['repetation'],
				'start_date' => $_GET['start_date'],
				'end_date' => $_GET['end_date']
			];

			$out = $this->db->insert("route_assignment", $data);
			if ($out) {
				echo json_encode($out);
			} else{
				echo json_encode(['status' => false]);
			}
		}
	}

	public function route_json(){
		$this->load->model('route_manag_model');

		$weeks = [
			'1' => 'first',
			'2' => 'second',
			'3' => 'third',
			'4' => 'fourth',
		];
		$days = [
			'1' => 'monday',
			'2' => 'tuesday',
			'3' => 'wednesday',
			'4' => 'thursday',
			'5' => 'friday',
			'6' => 'saturday',
			'7' => 'sunday',
		];
		$dates = [];
		$route_assign_list = $this->route_manag_model->route_assign_list();
		foreach ($route_assign_list as $value) {
			$dateforformat = date("Y-m", strtotime($value->start_date));
			$d = date("Y-m-d", strtotime($weeks[$value->week] . " " . $days[$value->day] . " " . $dateforformat . " "));
			// array_push($dates, $d);
			$dates[] = [
				'title' => 'Route Assign on this date',
				'start' => $d
			];
		}

		echo json_encode($dates);
	}


	public function route_calenderlist(){
		$data['main_content'] = 'layout/admin/pages/route_calender';
		$this->load->view('layout/admin/pages/route_calender');		
	}

	public function delete_calender()
	{
		$this->db->empty_table('route_assignment');
		redirect('route_assignment/route_calenderlist');
	}
}




 ?>