<?php 

class Home extends CI_Controller
{
		function __construct()
	{
		parent::__construct();
		$this->is_login_in();
	}
	
	public function is_login_in()
	{
		$is_login_in = $this->session->userdata('is_login_in');
		if(!isset($is_login_in) || $is_login_in != true)
		{
			echo "You don't have permission to access this page.";
            die();
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
        redirect('site');
	}

	public function index()
	{
		$data['main_content'] = 'layout/admin/pages/home';
		$this->load->view('admin_template', $data);
	}
}

 ?>